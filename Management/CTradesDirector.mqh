#include <TapeReading/Management/OrderManager.mqh>

class CTradesDirector
{
public:
    CTradesDirector(){};
    ~CTradesDirector();
    OrderManager* manager(int pIndex);
    void buy(const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
    void sell(const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
    void close();
    void closePartial();
    void attach(OrderManager *pOrderManager);
    int count() const;

private:
    OrderManager* m_orderManagers[];

};

CTradesDirector::~CTradesDirector() {
    for(int i = 0; i < count(); ++i) {
        if (CheckPointer(m_orderManagers[i]) != POINTER_INVALID) delete(m_orderManagers[i]);
    }
    ArrayFree(m_orderManagers);
}

void CTradesDirector::buy(const double pStopLoss, const double pTakeProfit, const string pComment) {
    for(int i = 0; i < ArraySize(m_orderManagers); ++i) {
        m_orderManagers[i].buy(pStopLoss, pTakeProfit, pComment);
    }
}

void CTradesDirector::sell(const double pStopLoss, const double pTakeProfit, const string pComment) {
    for(int i = 0; i < ArraySize(m_orderManagers); ++i) {
        m_orderManagers[i].sell(pStopLoss, pTakeProfit, pComment);
    }
}

void CTradesDirector::close() {
    for(int i = 0; i < ArraySize(m_orderManagers); ++i) {
        m_orderManagers[i].close();
    }
}

void CTradesDirector::closePartial() {
    for(int i = 0; i < ArraySize(m_orderManagers); ++i) {
        m_orderManagers[i].closePartial();
    }
}

void CTradesDirector::attach(OrderManager *pOrderManager) {
    for (int i = 0; i < ArraySize(m_orderManagers); ++i) {
        if (pOrderManager == m_orderManagers[i])
            return;
    }
    int newSize = ArraySize(m_orderManagers) + 1;
    ArrayResize(m_orderManagers, newSize);
    m_orderManagers[newSize - 1] = pOrderManager;
}

int CTradesDirector::count() const {
    return ArraySize(m_orderManagers);
}

OrderManager* CTradesDirector::manager(int pIndex) {
    return m_orderManagers[pIndex];
}