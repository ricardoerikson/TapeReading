#include <TapeReading/Dom/CMarketSide.mqh>
#include <TapeReading/Utils/MathUtils.mqh>

#include <Math/Fuzzy/mamdanifuzzysystem.mqh>


/*------------------------------------------------------------------
This inference system analyzes the favorability of aggressions go
into either bid or ask.
*/
class CSupportResistanceFIS
{
public:
    CSupportResistanceFIS(CMarketSide* pSide, int pDepth, double pMultiplier = 3.0, double pAboveFactor = 1.2);
    ~CSupportResistanceFIS();
    double evaluate();

private:
    void setInputs();
    void setOutputs();
    void setRules();

    CFuzzyVariable          *fvAbove;       // How many price levels are above mean
    CFuzzyVariable          *fbBarrier;
    CFuzzyVariable          *fvObstacle;            // Fuzzy variables for fvObstacle
    CMamdaniFuzzySystem     *mFs;                 // Mamdani Fuzzy Inference System
    CMarketSide             *m_side;               // Market side (bid or ask)
    CMamdaniFuzzyRule       *rule0, *rule1;

    const double m_multiplier, m_aboveFactor;
    const int m_depth;
};

CSupportResistanceFIS::CSupportResistanceFIS(CMarketSide* pSide, int pDepth, double pMultiplier, double pAboveFactor) :
        m_depth(pDepth), m_multiplier(pMultiplier), m_aboveFactor(pAboveFactor) {
    m_side = pSide;
    mFs = new CMamdaniFuzzySystem();
    setInputs();
    setOutputs();
    setRules();
}

CSupportResistanceFIS::~CSupportResistanceFIS() {
    if (CheckPointer(mFs) != POINTER_INVALID) delete(mFs);
}

void CSupportResistanceFIS::setInputs() {
    fvAbove = new CFuzzyVariable("above", 0.0, m_depth);
    fvAbove.Terms().Add(new CFuzzyTerm("few", new CTriangularMembershipFunction(0.0, 0.0, 2.0)));
    fvAbove.Terms().Add(new CFuzzyTerm("many", new CTrapezoidMembershipFunction(0.0, 2.0, m_depth, m_depth)));
    mFs.Input().Add(fvAbove);

    fbBarrier = new CFuzzyVariable("barrier", 0.0, 1.0);
    fbBarrier.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(0.0, 0.0, 1.0)));
    fbBarrier.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(0.0, 1.0, 1.0)));
    mFs.Input().Add(fbBarrier);
}

void CSupportResistanceFIS::setOutputs() {
    fvObstacle = new CFuzzyVariable("obstacle", 0.0, 1.0);
    fvObstacle.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(0.0, 0.0, 1.0)));
    fvObstacle.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(0.0, 1.0, 1.0)));
    mFs.Output().Add(fvObstacle);
}

void CSupportResistanceFIS::setRules() {
    rule0 = mFs.ParseRule("if (above is many) and (barrier is high) then (obstacle is high)"); // It is not the end of the queue yet.
    rule1 = mFs.ParseRule("if (barrier is low) then (obstacle is low)");
    mFs.Rules().Add(rule0);
    mFs.Rules().Add(rule1);
}

double CSupportResistanceFIS::evaluate() {
    double mean = m_side.averageVolume();
    double maxVolume = m_side.maxVolume(m_depth);
    int aboveCount = m_side.aboveMean(m_depth, m_aboveFactor);
    double barrier = ScaleValue(maxVolume / mean, 1.0, m_multiplier, 0.0, 1.0);

    CList *in = new CList;
    CDictionary_Obj_Double *p_od_above = new CDictionary_Obj_Double;
    CDictionary_Obj_Double *p_od_barrier = new CDictionary_Obj_Double;

    p_od_above.SetAll(fvAbove, aboveCount);
    p_od_barrier.SetAll(fbBarrier, barrier);
    in.Add(p_od_above);
    in.Add(p_od_barrier);
    CList* result;
    CDictionary_Obj_Double *p_od_fs;
    result = mFs.Calculate(in);
    p_od_fs = result.GetNodeAtIndex(0);
    double value = ScaleValue(p_od_fs.Value(), 1.0/3.0, 2.0/3.0, 0.0, 1.0);
    if (CheckPointer(in) != POINTER_INVALID) delete(in);
    if (CheckPointer(result) != POINTER_INVALID) delete(result);
    if (CheckPointer(p_od_above) != POINTER_INVALID) delete(p_od_above);
    if (CheckPointer(p_od_barrier) != POINTER_INVALID) delete(p_od_barrier);
    if (CheckPointer(p_od_fs) != POINTER_INVALID) delete(p_od_fs);
    return value;
}
