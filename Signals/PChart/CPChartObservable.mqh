#include <TapeReading/PChart/CPChart.mqh>

#include <TapeReading/Patterns/Observer/Observable.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>
#include <TapeReading/Core/CSymbol.mqh>

class CTimesNSales;

class CPChartObservable : public Observable<IScalpingObserver>
{
public:
    CPChartObservable(CSymbol &pSymbol, CTimesNSales *pTimesNSales, CPChart *pPChart);
    ~CPChartObservable();
    string name() const;
    void notifyObservers();

private:
    void notifyEnteringAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh);
    void notifyOutOfAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh);
    void notifyAccumulationZoneChanged(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh);

    CSymbol m_symbol;
    CPChart *m_pChart;

    CAggressionsBag *m_buyBag;
    CAggressionsBag *m_sellBag;

    double m_previousAsk, m_previousBid;
    double m_previousAccumLow, m_previousAccumHigh, m_previousAccumPrice;

};

CPChartObservable::CPChartObservable(CSymbol &pSymbol, CTimesNSales *pTimesNSales, CPChart *pPChart) :
        m_symbol(pSymbol) {
    m_pChart = pPChart;
}

CPChartObservable::~CPChartObservable() {
    if (CheckPointer(m_buyBag) != POINTER_INVALID) delete(m_buyBag);
    if (CheckPointer(m_sellBag) != POINTER_INVALID) delete(m_sellBag);
}

string CPChartObservable::name() const {
    return "P Chart Observable";
}

void CPChartObservable::notifyObservers() {
    bool notifyOnAccumulationZone = false;
    double accumPriceLow = m_pChart.bar().accumulation().low();
    double accumPriceHigh = m_pChart.bar().accumulation().high();
    double accumPrice = m_pChart.bar().accumulation().price();
    double priceLow = m_pChart.bar().low();
    double priceHigh = m_pChart.bar().high();
    notifyEnteringAccumulationZone(accumPriceLow, accumPriceHigh, accumPrice, priceLow, priceHigh);
    notifyOutOfAccumulationZone(accumPriceLow, accumPriceHigh, accumPrice, priceLow, priceHigh);
    notifyAccumulationZoneChanged(accumPriceLow, accumPriceHigh, accumPrice, priceLow, priceHigh);
    m_previousAsk = SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK);
    m_previousBid = SymbolInfoDouble(m_symbol.name(), SYMBOL_BID);
    m_previousAccumPrice = accumPrice;
    m_previousAccumLow = accumPriceLow;
    m_previousAccumHigh = accumPriceHigh;
}

void CPChartObservable::notifyEnteringAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh) {
    if (pAccumLow == 0 || pAccumHigh == 0 || m_pChart.bar().depth() < 4 || m_pChart.bar().accumulation().depth() < 2)
        return;
    double currentAsk = SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK);
    double currentBid = SymbolInfoDouble(m_symbol.name(), SYMBOL_BID);
    int direction = 0;
    if (m_previousBid < currentBid && currentBid == pAccumLow && m_previousAccumLow == pAccumLow)
        direction = 1;
    if (m_previousAsk > currentAsk && currentAsk == pAccumHigh && m_previousAccumHigh == pAccumHigh)
        direction = -1;
    if (direction != 0 && TicksCounter(m_symbol, currentAsk, currentBid) == 1) {
        for(int i = 0; i < ArraySize(m_observers); i++)
            m_observers[i].onEnteringAccumulationZone(pAccumLow, pAccumHigh, pAccumPrice, pLow, pHigh, direction);
    }
}

void CPChartObservable::notifyOutOfAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh) {
    if (pAccumLow == 0 || pAccumHigh == 0)
        return;
    double currentAsk = SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK);
    double currentBid = SymbolInfoDouble(m_symbol.name(), SYMBOL_BID);
    int direction = 0;
    if (currentBid < m_previousBid && m_previousBid == pAccumLow)
        direction = -1;
    if (currentAsk > m_previousAsk && m_previousAsk == pAccumHigh)
        direction = 1;
    if (direction != 0) {
        for (int i = 0; i < ArraySize(m_observers); i++)
            m_observers[i].onOutOfAccumulationZone(pAccumLow, pAccumHigh, pAccumPrice, pLow, pHigh, direction);
    }
}

void CPChartObservable::notifyAccumulationZoneChanged(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh) {
    if (pAccumLow == 0 || pAccumHigh == 0)
        return;
    bool notifyAccumulationZoneChanged = false;
    bool accumLowChanged = (pAccumLow != m_previousAccumLow);
    bool accumHighChanged = (pAccumHigh != m_previousAccumHigh);
    bool accumPriceChanged = (pAccumPrice != m_previousAccumPrice);

    if ( accumLowChanged || accumHighChanged || accumPriceChanged)
        for (int i = 0; i < ArraySize(m_observers); i++)
            m_observers[i].onAccumulationZoneChanged(pAccumLow, pAccumHigh, pAccumPrice, pLow, pHigh);
}

