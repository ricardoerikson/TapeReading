#include <TapeReading/Core/CSymbol.mqh>
#include <TapeReading/Patterns/Observer/Observable.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>
#include <TapeReading/Signals/EndOfQueue/CEndOfQueueFIS.mqh>
#include <TapeReading/Dom/CDepthOfMarket.mqh>
#include <TapeReading/Utils/TickUtils.mqh>


class CEndOfQueueObservable : public Observable<IScalpingObserver>
{
public:
    CEndOfQueueObservable(CSymbol &pSymbol, CDepthOfMarket *pDom, double pMinVolume, double pMaxVolume, double pEoqFactor = 0.0);
    void notifyObservers();
    string name() const;
    void setAskListener(double pPrice);
    void setBidListener(double pPrice);
    double evaluateAsk() const;
    double evaluateBid() const;

private:
    void notifyEndOfQueueAtBid();
    void notifyEndOfQueueAtAsk();

    CDepthOfMarket *m_dom;
    CSymbol m_symbol;

    const double m_minVolume, m_maxVolume;
    const double m_eoqFactor; // End of queue factor
    double m_askListener, m_bidListener;

};

CEndOfQueueObservable::CEndOfQueueObservable(CSymbol &pSymbol, CDepthOfMarket *pDom, double pMinVolume, double pMaxVolume, double pEoqFactor) :
    m_symbol(pSymbol), m_minVolume(pMinVolume), m_maxVolume(pMaxVolume),
    m_eoqFactor(pEoqFactor) {
    m_dom = pDom;
    m_askListener = WRONG_VALUE;
    m_bidListener = WRONG_VALUE;
}

string CEndOfQueueObservable::name() const {
    return "End of Queue Observable";
}

double CEndOfQueueObservable::evaluateAsk() const {
    CEndOfQueueFIS fis(m_minVolume, m_maxVolume, m_dom.ask().volume(0));
    return fis.evaluate();
}

double CEndOfQueueObservable::evaluateBid() const {
    CEndOfQueueFIS fis(m_minVolume, m_maxVolume, m_dom.bid().volume(0));
    return fis.evaluate();
}

void CEndOfQueueObservable::setBidListener(double pPrice) {
    m_bidListener = pPrice;
}

void CEndOfQueueObservable::setAskListener(double pPrice) {
    m_askListener = pPrice;
}

void CEndOfQueueObservable::notifyEndOfQueueAtBid() {
    if (m_bidListener == WRONG_VALUE)
        return;
    double bid = m_dom.bid().bestPrice();
    if (m_bidListener == bid) {
        double evalBid = evaluateBid();
        if (evalBid <= m_eoqFactor) {
            for (int i = 0; i < observersCount(); ++i) {
                observer(i).onEndOfQueueAtBid(m_bidListener, m_dom.bid().volume(0), evalBid);
            }
            m_bidListener = WRONG_VALUE;
        }
    } else if (m_bidListener > bid || TicksCounter(m_symbol, m_bidListener, bid) > 1) {
        m_bidListener = WRONG_VALUE;
    }
}

void CEndOfQueueObservable::notifyEndOfQueueAtAsk() {
    if (m_askListener == WRONG_VALUE)
        return;
    double ask = m_dom.ask().bestPrice();
    if (m_askListener == ask) {
        double evalAsk = evaluateAsk();
        if (evalAsk <= m_eoqFactor) {
            for (int i = 0; i < observersCount(); ++i) {
                observer(i).onEndOfQueueAtAsk(m_askListener, m_dom.ask().volume(0), evalAsk);
            }
            m_askListener = WRONG_VALUE;
        }
    } else if (m_askListener < ask || TicksCounter(m_symbol, m_askListener, ask) > 1) {
        m_askListener = WRONG_VALUE;
    }
}

void CEndOfQueueObservable::notifyObservers() {
    notifyEndOfQueueAtBid();
    notifyEndOfQueueAtAsk();
}
