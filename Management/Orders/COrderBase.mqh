#include <TapeReading/Core/CSymbol.mqh>
#include <TapeReading/Management/Trade/TradeReport.mqh>
#include <TapeReading/Management/Orders/IOrderListener.mqh>
#include <Trade/Trade.mqh>

class COrderBase
{
public:
    COrderBase(const CSymbol &pSymbol, const string pAction, IOrderListener *pListener = NULL);
    bool execute(const uint pLots, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);

protected:
    virtual bool operation(const double pVolume, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) = 0;
    string attempt();
    string success();
    string error();
    CTrade m_trade;
    CSymbol m_symbol;

private:
    const string m_action;
    MqlTradeResult m_result;
    MqlTradeCheckResult m_check;
    IOrderListener *m_listener;


};

COrderBase::COrderBase(const CSymbol &pSymbol, const string pAction, IOrderListener *pListener = NULL) :
     m_symbol(pSymbol),
     m_action(pAction) {
    m_listener = pListener;
}

bool COrderBase::execute(const uint pLots, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    ReportAction(m_symbol, attempt(), pComment);
    double expectedVolume = pLots * m_symbol.lotsStep();
    double volume = m_trade.CheckVolume(m_symbol.name(), expectedVolume, pPrice, ORDER_TYPE_BUY);
    if (volume == 0) {
        MqlTradeCheckResult checkResult;
        m_trade.CheckResult(checkResult);
        ReportResult(m_symbol, checkResult, error(), pComment);
        return (false);
    }

    bool success = operation(volume, pPrice, pStopLoss, pTakeProfit, pComment);
    MqlTradeResult result;
    m_trade.Result(result);
    if (success) {
        ReportResult(m_symbol, result, success(), pComment);
        if (CheckPointer(m_listener) != POINTER_INVALID)
            m_listener.onSuccess((uint)(m_trade.ResultVolume() / m_symbol.lotsStep()));
    } else {
        ReportResult(m_symbol, result, error(), pComment);
    }
    return(success);
}

string COrderBase::attempt() {
    return StringFormat("%s_%s", m_action, "ATTEMPT");
}

string COrderBase::success() {
    return StringFormat("%s_%s", m_action, "SUCCESS");
}

string COrderBase::error() {
    return StringFormat("%s_%s", m_action, "ERROR");
}
