class CAggression;

interface IScalpingObserver {

    /*------------------------------------------------------------------
    Called when there is a partially filled buy market order and possibly
    there are some remaining contracts converted to a limit order
    at the execution price (last market price) obtained by the initial
    partial fill.
    */
    void onLeftoverOnBid(double pLeftoverVolume, double pExecutionPrice);

    /*------------------------------------------------------------------
    Called when there is a partially filled sell market order and possibly
    there are some remaining contracts converted to a limit order
    at the execution price (last market price) obtained by the initial
    partial fill.
    */
    void onLeftoverOnAsk(double pLeftoverVolume, double pExecutionPrice);

    /*------------------------------------------------------------------
    Called when a price level (bid or ask) is runing out of contracts.
    */
    void onEndOfQueueAtAsk(double pPrice, double pVolume, double pFactor);
    void onEndOfQueueAtBid(double pPrice, double pVolume, double pFactor);

    /*------------------------------------------------------------------
    Called when hidden liquidity is identified at bid price level.
    */
    void onHiddenLiquidityAtBid(double pPrice, double pVolume);

    /*------------------------------------------------------------------
    Called when hidden liquidity is identified at ask price level.
    */
    void onHiddenLiquidityAtAsk(double pPrice, double pVolume);

    /*------------------------------------------------------------------
    Listener that is called every time occurs a change in spread. This
    method is called only when the new ask and new bid are different
    from the previous ones. That is, both of the values must be
    different.

    @params
        when        Time when spread changed
        bid         New best bid
        ask         New best ask
    */
    void onSpreadChanged(datetime pWhen, double pBid, double pAsk, int pDirection);
    /*------------------------------------------------------------------
    Listener that is called when the spread is in acceptance. This
    method can be called many times before a change in the spread.
    @params
        since       Time when occurred the last change in the spread
        elapsedTime Time elapsed since last change in spread (in seconds)
        bid         Current best bid
        ask         Current best ask
    */
    void onSpreadAcceptance(datetime pSince, int pElapsedTime, double pLastBid, double pLastAsk);
    /*------------------------------------------------------------------
    Listener that is called only when spread is open. When this listener
    is called, it passes both best bid and best ask at the moment.
    @params
        bestBid     Current best bid
        bestAsk     Current best ask
    */
    void onSpreadOpened(double pBestBid, double pBestAsk);

    /*------------------------------------------------------------------
    Listener that is called when changes of spread entering the value
    zone are perceived in a p bar.
    */
    void onEnteringAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh, int pDirection);

    /*------------------------------------------------------------------
    Listener that is called when changes of spread going out of the value
    zone are perceived in a p bar.
    */
    void onOutOfAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh, int pDirection);

    /*------------------------------------------------------------------
    Listener that is called when changes in the prices of the value
    zone are perceived in a p bar.
    */
    void onAccumulationZoneChanged(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh);

    /*------------------------------------------------------------------
    Callback that is called when imbalance is higher than the imbalance
    factor defined in the constructor.
    */
    void onDepthImbalance(double pImbalance);

    void onNextToSupportLevel(double pSupport);
    void onNextToResistanceLevel(double pResistance);
    void onTheVergeOfBreakingSupportLevel(double pSupport, double pImbalance);
    void onTheVergeOfBreakingResistanceLevel(double pResistance, double pImbalance);

    void onLargeAggression(CAggression &pAggression);

    void onFrontRunningBuy();
    void onFrontRunningSell();

};