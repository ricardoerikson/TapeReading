#include <TapeReading/Core/CSymbol.mqh>
#include <TapeReading/Dom/CDepthOfMarket.mqh>

class CStrategy
{
public:
    CStrategy(CSymbol &pSymbol, CDepthOfMarket *pDom);
    ~CStrategy();
    virtual bool hold() = 0;
    void buyLimit(double pPrice);
    void sellLimit(double pPrice);
    void buyMarket(double pPrice);
    void sellMarket(double pPrice);
    void bidEndOfQueue(double pPrice);
    void askEndOfQueue(double pPrice);
    void sellLimitTicket(double pTicket);
    void buyLimitTicket(double pTicket);
    double buyLimit() const;
    double sellLimit() const;
    double buyMarket() const;
    double sellMarket() const;
    double bidEndOfQueue() const;
    double askEndOfQueue() const;
    double sellLimitTicket() const;
    double buyLimitTicket() const;

protected:
    CSymbol m_symbol;
    CDepthOfMarket* dom();

private:
    CDepthOfMarket *m_dom;
    double m_buyLimit;
    double m_sellLimit;
    double m_buyMarket;
    double m_sellMarket;
    double m_bidEndOfQueue;
    double m_askEndOfQueue;
    double m_buyLimitTicket;
    double m_sellLimitTicket;
};

CStrategy::CStrategy(CSymbol &pSymbol, CDepthOfMarket *pDom) :
        m_symbol(pSymbol),
        m_buyLimit(WRONG_VALUE),
        m_sellLimit(WRONG_VALUE),
        m_buyMarket(WRONG_VALUE),
        m_sellMarket(WRONG_VALUE),
        m_bidEndOfQueue(WRONG_VALUE),
        m_askEndOfQueue(WRONG_VALUE),
        m_buyLimitTicket(WRONG_VALUE),
        m_sellLimitTicket(WRONG_VALUE)
{
    m_dom = pDom;
}

CStrategy::~CStrategy() {}

void CStrategy::buyLimit(double pPrice) {
    m_buyLimit = pPrice;
}

void CStrategy::sellLimit(double pPrice) {
    m_sellLimit = pPrice;
}

void CStrategy::buyMarket(double pPrice) {
    m_buyMarket = pPrice;
}

void CStrategy::sellMarket(double pPrice) {
    m_sellMarket = pPrice;
}

void CStrategy::bidEndOfQueue(double pPrice) {
    m_bidEndOfQueue = pPrice;
}

void CStrategy::askEndOfQueue(double pPrice) {
    m_askEndOfQueue = pPrice;
}

void CStrategy::buyLimitTicket(double pTicket) {
    m_buyLimitTicket = pTicket;
}

void CStrategy::sellLimitTicket(double pTicket) {
    m_sellLimitTicket = pTicket;
}

double CStrategy::buyLimit() const {
    return m_buyLimit;
}
double CStrategy::sellLimit() const {
    return m_sellLimit;
}
double CStrategy::buyMarket() const {
    return m_buyMarket;
}
double CStrategy::sellMarket() const {
    return m_sellMarket;
}
double CStrategy::bidEndOfQueue() const {
    return m_bidEndOfQueue;
}
double CStrategy::askEndOfQueue() const {
    return m_askEndOfQueue;
}
double CStrategy::sellLimitTicket() const {
    return m_sellLimitTicket;
}
double CStrategy::buyLimitTicket() const {
    return m_buyLimitTicket;
}






