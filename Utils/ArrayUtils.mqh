void ArrayClip(double &array[], double lower, double upper) {
    for (int i = 0; i < ArraySize(array); i++) {
        if (array[i] < lower)
            array[i] = lower;
        if (array[i] > upper)
            array[i] = upper;
    }
}

template <typename T>
void ArrayRemoveIndex(T &MyArray[],int index)
{
   T TempArray[];
   ArrayCopy(TempArray,MyArray,0,0,index);
   ArrayCopy(TempArray,MyArray,index,(index+1));
   ArrayFree(MyArray);
   ArrayCopy(MyArray,TempArray,0,0);
   ArrayFree(TempArray);
}

template <typename T>
void QuickSort(T &pValues[], int pLo, int pHi, double &pPrices[]) {
    if (pLo < pHi) {
        int p = QuickSortPartition(pValues, pLo, pHi, pPrices);
        QuickSort(pValues, pLo, p - 1, pPrices);
        QuickSort(pValues, p + 1, pHi, pPrices);
    }
}

template <typename T>
int QuickSortPartition(T &pValues[], int pLo, int pHi, double &pPrices[]) {
    T pivot = pValues[pHi];
    int i = pLo - 1;
    for (int j = pLo; j <= pHi; j++) {
        if (pValues[j] <= pivot) {
            i = i + 1;
            if (i != j) {
                T temp = pValues[i];
                pValues[i] = pValues[j];
                pValues[j] = temp;
                double price = pPrices[i];
                pPrices[i] = pPrices[j];
                pPrices[j] = price;
            }
        }
    }
    return i;
}