#include <TapeReading/Patterns/Observer/Observable.mqh>

#include <TapeReading/Dom/CDepthOfMarket.mqh>
#include <TapeReading/Signals/DepthImbalance/CDepthImbalanceFIS.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>

class CDepthImbalanceObservable : public Observable<IScalpingObserver>
{
public:
    CDepthImbalanceObservable(CDepthOfMarket *pDom, int pFirstLevel = 3, int pSecondLevel = 5, double pImbalanceFactor = 0.5, bool pIncludeFirst = true);
    ~CDepthImbalanceObservable(){};
    string name() const;
    double imbalance(int pFirstLevel, int pSecondLevel, bool pIncludeFirst = true);
    void notifyObservers();

private:
    CDepthOfMarket *m_dom;

    bool m_includeFirst;
    int m_firstLevel, m_secondLevel;
    double m_imbalanceFactor;

};

CDepthImbalanceObservable::CDepthImbalanceObservable(CDepthOfMarket *pDom, int pFirstLevel, int pSecondLevel, double pImbalanceFactor, bool pIncludeFirst):
        m_firstLevel(pFirstLevel),
        m_secondLevel(pSecondLevel),
        m_imbalanceFactor(pImbalanceFactor),
        m_includeFirst(pIncludeFirst) {
    m_dom = pDom;
}

string CDepthImbalanceObservable::name() const {
    return "Depth Imbalance Observable";
}

double CDepthImbalanceObservable::imbalance(int pFirstLevel, int pSecondLevel, bool pIncludeFirst) {
    double firstLevelImbalance  = m_dom.imbalance(pFirstLevel, pIncludeFirst);
    double secondLevelImbalance = m_dom.imbalance(pSecondLevel, pIncludeFirst);
    CDepthImbalanceFIS fis(firstLevelImbalance, secondLevelImbalance);
    return fis.evaluate();
}

void CDepthImbalanceObservable::notifyObservers() {
    double depthImbalance = imbalance(m_firstLevel, m_secondLevel, m_includeFirst);
    if (MathAbs(depthImbalance) > m_imbalanceFactor) {
        for (int i = 0; i < ArraySize(m_observers); ++i) {
            m_observers[i].onDepthImbalance(depthImbalance);
        }
    }
}