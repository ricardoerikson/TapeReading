#include <TapeReading/Patterns/Observer/Observable.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>
#include <TapeReading/TimesNSales/CTimesNSales.mqh>
#include <TapeReading/Utils/TickUtils.mqh>
#include <TapeReading/Core/Enums.mqh>
#include <TapeReading/Core/CAggression.mqh>

class CLargeAggressionObservable : public Observable<IScalpingObserver>
{
public:
    CLargeAggressionObservable(CTimesNSales *pTimesNSales, int pFilter = 80);
    ~CLargeAggressionObservable();

    double volumeImbalance() const;
    void notifyObservers();
    void reset();
    string name() const;

private:
    void notify(double pLast, double pVolume, AGGRESSION_TYPE pType, datetime pDatetime);

    CTimesNSales *m_timesNSales;
    int m_filter;

};

CLargeAggressionObservable::CLargeAggressionObservable(CTimesNSales *pTimesNSales, int pFilter) :
        m_filter(pFilter) {
    m_timesNSales = pTimesNSales;
}

CLargeAggressionObservable::~CLargeAggressionObservable() {
}


void CLargeAggressionObservable::notifyObservers() {
    if (ArraySize(m_timesNSales.trades) <= 0)
        return;
    double lastBuy = 0.0;
    double lastSell = 0.0;
    double sumBuy = 0;
    double sumSell = 0;
    for (int i = 0; i < ArraySize(m_timesNSales.trades); ++i) {
        MqlTick tick = m_timesNSales.trades[i];
        if (IsBetween(tick)) {
            continue;
        }
        if (IsBuyAggression(tick)) {
            sumBuy += (double) tick.volume;
            lastBuy = tick.last;
            continue;
        }

        if (IsSellAggression(tick)) {
            sumSell += (double) tick.volume;
            lastSell = tick.last;
            continue;
        }
    }
    if (sumBuy >= m_filter) notify(lastBuy, sumBuy, BUY_AGGRESSION, TimeCurrent());
    if (sumSell >= m_filter) notify(lastSell, sumSell, SELL_AGGRESSION, TimeCurrent());
}

void CLargeAggressionObservable::notify(double pLast, double pVolume, AGGRESSION_TYPE pType, datetime pDatetime) {
    CAggression aggression(pLast, pVolume, pType, pDatetime);
    for (int i = 0; i < observersCount(); ++i) {
        observer(i).onLargeAggression(aggression);
    }
}

string CLargeAggressionObservable::name() const {
    return "Large Aggression Observable";
}