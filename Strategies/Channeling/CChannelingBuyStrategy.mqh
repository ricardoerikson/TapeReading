#include <TapeReading/Core/CSymbol.mqh>
#include <TapeReading/Dom/CDepthOfMarket.mqh>
#include <TapeReading/Utils/TickUtils.mqh>
#include <TapeReading/Strategies/CStrategy.mqh>

class CChannelingBuyStrategy : public CStrategy
{
public:
    CChannelingBuyStrategy(CSymbol  &pSymbol, CDepthOfMarket *pDom, int pMinVolume = 40);
    ~CChannelingBuyStrategy();
    bool hold();

private:
    int m_minVolume;
    double m_minBid;
    double m_maxAsk;
};

CChannelingBuyStrategy::CChannelingBuyStrategy(CSymbol &pSymbol, CDepthOfMarket *pDom, int pMinVolume) :
        CStrategy(pSymbol, pDom),
        m_minVolume(pMinVolume) {
}

bool CChannelingBuyStrategy::hold() {
    double bid = dom().bid().bestPrice();
    double ask = dom().ask().bestPrice();
    if (bid < m_minBid)
        m_minBid = bid;
    if (ask > m_maxAsk)
        m_maxAsk = ask;

    // Price is falling rule
    if (bid < buyLimit()) {
        int ticksDiff = TicksCounter(m_symbol, bid, buyLimit());
        if (ticksDiff < 1) {
            return false;
        } else if (ticksDiff == 1 && dom().bid().volume(0) <= m_minVolume) {
            return false;
        }
    }

    if (bid > buyLimit() && !PositionSelect(m_symbol.name())) {
        return false;
    }

    // Price went up and then fell
    if (TicksCounter(m_symbol, bid, m_minBid) > 1 && bid == buyLimit()) {
        return false;
    }

    if (ask >= sellLimit() && !PositionSelect(m_symbol.name())) {
        return false;
    }

    if (m_maxAsk == sellLimit() && TicksCounter(m_symbol, m_maxAsk, ask) >= 1) {
        return false;
    }

    return true;
}