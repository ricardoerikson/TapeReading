#include <TapeReading/Management/Trade/ECheckRetcode.mqh>
#include <TapeReading/Management/Trade/TradeUtils.mqh>
#include <TapeReading/Core/CSymbol.mqh>
#include <errordescription.mqh>

bool IsReportEnabled() {
    bool reportEnabled = false;
    if (GlobalVariableCheck("_glReport"))
        reportEnabled = GlobalVariableGet("_glReport");
    return reportEnabled;
}

enum TRADE_REPORT_MODE {
    TRADE_REPORT_PRINT = 1,
    TRADE_REPORT_ALERT = 2,
    TRADE_REPORT_COMMENT = 4,
    TRADE_REPORT_NOTIFICATION = 8,
    TRADE_REPORT_EMAIL = 16
};

void ReportSecurityCheck(string pAction, string pComment = NULL) {
    string comment = (pComment == NULL) ? "" : StringFormat("<%s>", pComment);
    string strTemplate = "SECURITY_CHECK_FAILED - [%s] %s";
    string report = StringFormat(strTemplate, pAction, comment);
    Report(TRADE_REPORT_ALERT | TRADE_REPORT_NOTIFICATION | TRADE_REPORT_EMAIL, report, pComment);
}

void ReportAction(CSymbol &pSymbol, string pAction, string pComment = NULL, int pMode = TRADE_REPORT_PRINT) {
    if (!IsReportEnabled())
        return;
    string comment = (pComment == NULL) ? "" : StringFormat("<%s>", pComment);
    string strTemplate = "@%s - [%s] %s";
    string report = StringFormat(strTemplate, pSymbol.name(), pAction, comment);
    Report(pMode, report, pComment);
}

void ReportResult(CSymbol &pSymbol, MqlTradeCheckResult &pResult, string pAction, string pComment = NULL, int pMode = TRADE_REPORT_PRINT) {
    if (!IsReportEnabled())
        return;
    string resultDescription = TradeServerReturnCodeDescription(pResult.retcode);
    string comment = (pComment == NULL) ? "" : StringFormat("<%s>", pComment);
    string strTemplate = "@%s - [%s] Result: %I64u - %s %s";
    string report = StringFormat(strTemplate, pSymbol.name(), pAction, pResult.retcode, resultDescription, comment);
    Report(pMode, report, pComment);
}

void ReportResult(CSymbol &pSymbol, MqlTradeResult &pResult, string pAction, string pComment = NULL, int pMode = TRADE_REPORT_PRINT) {
    if (!IsReportEnabled())
        return;
    string resultDescription = TradeServerReturnCodeDescription(pResult.retcode);
    string comment = (pComment == NULL) ? "" : StringFormat("<%s>", pComment);
    string strTemplate = "@%s - [%s] Result: %I64u - %s %s";
    string report = StringFormat(strTemplate, pSymbol.name(), pAction, pResult.retcode, resultDescription, comment);
    Report(pMode, report, pComment);
}

void Report(int pMode, string pReport, string pComment) {
    if ((TRADE_REPORT_PRINT & pMode) == TRADE_REPORT_PRINT) Print(pReport);
    if ((TRADE_REPORT_ALERT & pMode) == TRADE_REPORT_ALERT) Alert(pReport);
    if (((TRADE_REPORT_COMMENT & pMode) == TRADE_REPORT_COMMENT) && (pComment != NULL)) Comment(pComment);
    if ((TRADE_REPORT_NOTIFICATION & pMode) == TRADE_REPORT_NOTIFICATION) SendNotification(pReport);
    if ((TRADE_REPORT_EMAIL & pMode) == TRADE_REPORT_EMAIL) SendMail(pReport, pComment);
}