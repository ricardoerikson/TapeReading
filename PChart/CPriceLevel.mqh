class CPriceLevel
{
public:
    CPriceLevel();
    ~CPriceLevel();
    double buyVolume();
    double sellVolume();
    void addBuyVolume(double volume);
    void addSellVolume(double volume);
    double total();
    double delta();

private:
    double mBuyVolume, mSellVolume;

};

CPriceLevel::CPriceLevel() :
    mBuyVolume(0), mSellVolume(0){
}

CPriceLevel::~CPriceLevel() {
}

void CPriceLevel::addBuyVolume(double volume) {
    mBuyVolume += volume;
}

void CPriceLevel::addSellVolume(double volume) {
    mSellVolume += volume;
}

double CPriceLevel::buyVolume() {
    return mBuyVolume;
}

double CPriceLevel::sellVolume() {
    return mSellVolume;
}

double CPriceLevel::total() {
    return mBuyVolume + mSellVolume;
}

double CPriceLevel::delta() {
    return mBuyVolume - mSellVolume;
}