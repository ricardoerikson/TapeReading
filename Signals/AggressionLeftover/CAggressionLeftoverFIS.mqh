#include <TapeReading/Utils/MathUtils.mqh>

#include <Math/Fuzzy/mamdanifuzzysystem.mqh>


/*------------------------------------------------------------------
Fuzzy Inference System for price volume. This inference system
analyzes the volume given the mean of a market side.
*/
class CAggressionLeftoverFIS
{
public:
    CAggressionLeftoverFIS(double mean, double volume);
    ~CAggressionLeftoverFIS();
    double evaluate();

private:
    void setInputs();
    void setOutputs();
    void setRules();
    double scaleVolume(double mean, double volume);
    CFuzzyVariable          *fvVolume;          // Fuzzy variable for price volume
    CFuzzyVariable          *fvStatus;          // Fuzzy variables for fvStatus
    CMamdaniFuzzySystem     *mFs;               // Mamdani Fuzzy Inference System
    CMamdaniFuzzyRule       *rule0, *rule1;
    const double MAX_INFERENCED_VALUE, MIN_INFERENCED_VALUE;
    double mMean, mVolume;
};

CAggressionLeftoverFIS::CAggressionLeftoverFIS(double mean, double volume):
   MAX_INFERENCED_VALUE(2.0/3.0), MIN_INFERENCED_VALUE(1.0/3.0), mMean(mean) {
    mVolume = MathMax(0, volume);
    mFs = new CMamdaniFuzzySystem();
    setInputs();
    setOutputs();
    setRules();
}

CAggressionLeftoverFIS::~CAggressionLeftoverFIS() {
    if (CheckPointer(mFs) != POINTER_INVALID) delete(mFs);
}

void CAggressionLeftoverFIS::setInputs() {
    fvVolume = new CFuzzyVariable("volume", 0.0, 100.0);
    fvVolume.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(0.0, 0.0, 50.0)));
    fvVolume.Terms().Add(new CFuzzyTerm("average", new CTriangularMembershipFunction(0.0, 50.0, 100.0)));
    fvVolume.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(50.0, 100.0, 100.0)));
    mFs.Input().Add(fvVolume);
}

void CAggressionLeftoverFIS::setOutputs() {
    fvStatus = new CFuzzyVariable("status", 0.0, 1.0);
    fvStatus.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(0.0, 0.0, 1.0)));
    fvStatus.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(0.0, 1.0, 1.0)));
    mFs.Output().Add(fvStatus);
}

void CAggressionLeftoverFIS::setRules() {
    rule0 = mFs.ParseRule("if (volume is not low) then (status is high)"); // It is not the end of the queue yet.
    rule1 = mFs.ParseRule("if (volume is low) then (status is low)");
    mFs.Rules().Add(rule0);
    mFs.Rules().Add(rule1);
}

/*------------------------------------------------------------------
Normalize values to a value ranging  from 0 to 100.
*/
double CAggressionLeftoverFIS::scaleVolume(double mean, double volume) {
    double maxVol = 2 * mean;
    double boundedVolume = MathMax(0, MathMin(volume, maxVol));
    return ScaleValue(boundedVolume, 0, maxVol, 0, 100);
}

double CAggressionLeftoverFIS::evaluate() {
    CList *in = new CList;
    CDictionary_Obj_Double *p_od_volume = new CDictionary_Obj_Double;

    double val = scaleVolume(mMean, mVolume);
    p_od_volume.SetAll(fvVolume, val);
    in.Add(p_od_volume);
    CList* result;
    CDictionary_Obj_Double *p_od_fs;
    result = mFs.Calculate(in);
    p_od_fs = result.GetNodeAtIndex(0);
    double value = ScaleValue(p_od_fs.Value(), MIN_INFERENCED_VALUE, MAX_INFERENCED_VALUE, 0.0, 1.0);
    if (CheckPointer(in) != POINTER_INVALID) delete(in);
    if (CheckPointer(result) != POINTER_INVALID) delete(result);
    if (CheckPointer(p_od_volume) != POINTER_INVALID) delete(p_od_volume);
    if (CheckPointer(p_od_fs) != POINTER_INVALID) delete(p_od_fs);
    return value;
}
