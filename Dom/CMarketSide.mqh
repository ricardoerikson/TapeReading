#include <TapeReading/Dom/CMarketBook.mqh>
#include <TapeReading/Utils/ArrayUtils.mqh>
#include <TapeReading/Core/CSymbol.mqh>
#include <Math/Stat/Math.mqh>

class CMarketSide
{
public:
    CMarketSide(CMarketBook *pBook, const CSymbol &pSymbol, double pMaxDeviation);

    /*------------------------------------------------------------------
    Returns the amount of price levels that are above (mean * pFactor).
    */
    int aboveMean(int pDepth = 4, double pFactor = 1.2);
    int bestIndex();
    int lastIndex();
    int depth();
    double worstPrice();
    bool exists();
    virtual bool hasLiquidityVacuum(int pDepth);
    virtual double bestPrice();
    virtual double deviation(long pVolume) = 0;
    virtual double maxVolume(int pDepth) {return 0.0;};
    virtual double maxVolumePrice(int pDepth = 4) { return 0.0; };
    virtual int maxVolumeIndex(int pDepth = 4);
    /*------------------------------------------------------------------
    Returns the volume at a given position of the market side.

    Arguments:
        pos -       index of the side where 0 is the best index. Observe
                    that this argument corresponds to the price levels
                    that have liquidity.

    Returns:
        Volume at the given position.
    */
    virtual double volume(int pPos){return 0;};
    virtual double averageVolume(int pDepth = 8) { return 0; };
    virtual double accumulatedVolume(int pDepth){ return 0; };

    CMarketSide* bestIndex(int pBestIndex);
    CMarketSide* lastIndex(int pLastIndex);

protected:
    double average(double &pArray[]);
    CMarketBook *m_marketBook;
    int m_bestIndex, m_lastIndex;
    double m_maxDeviation;
    const CSymbol m_symbol;
};

CMarketSide::CMarketSide(CMarketBook *pBook, const CSymbol &pSymbol, double pMaxDeviation):
    m_bestIndex(-1), m_lastIndex(-1), m_maxDeviation(pMaxDeviation), m_symbol(pSymbol) {
    m_marketBook = pBook;
}

int CMarketSide::bestIndex() {
    if (exists())
        return m_bestIndex;
    return -1;
}

int CMarketSide::lastIndex() {
    if (exists())
        return m_lastIndex;
    return -1;
}

double CMarketSide::worstPrice() {
    if (exists())
        return m_marketBook.book[m_lastIndex].price;
    return -1.0;
}

CMarketSide* CMarketSide::bestIndex(int pBestIndex) {
    m_bestIndex = pBestIndex;
    return GetPointer(this);
}

CMarketSide* CMarketSide::lastIndex(int pLastIndex) {
    m_lastIndex = pLastIndex;
    return GetPointer(this);
}

bool CMarketSide::exists() {
    return m_bestIndex != -1;
}

int CMarketSide::depth() {
    if (exists())
        return MathAbs(bestIndex() - lastIndex()) + 1;
    return 0;
}

double CMarketSide::average(double &pArray[]) {
    int count = 0;
    while (MathStandardDeviation(pArray) > m_maxDeviation) {
        if (count > 5) {
            Print("Forced Break.");
            return -1.0;
        }
        double mean = MathMean(pArray);
        double std = MathStandardDeviation(pArray);
        ArrayClip(pArray, mean + std + m_symbol.lotsStep(), mean - std - m_symbol.lotsStep());
        count++;
    }
    int mean = (int)MathMean(pArray);
    return (double)(mean - mean % (int) m_symbol.lotsStep());
}

int CMarketSide::aboveMean(int pDepth, double pFactor) {
    double mean = averageVolume();
    int count = 0;
    for (int i = 0; i < MathMin(pDepth, depth()); ++i)
        if (volume(i) > pFactor * mean)
            ++count;
    return count;
}
