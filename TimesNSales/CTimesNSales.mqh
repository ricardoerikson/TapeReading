#include <TapeReading/Utils/CDatetime.mqh>
#include <TapeReading/Core/CSymbol.mqh>

class CTimesNSales
{
public:
    CTimesNSales(CSymbol &symbol);
    ~CTimesNSales();
    bool retrieveSince(datetime since, int ticksCount = 1000);
    MqlTick trades[];

private:
    CSymbol m_symbol;

    datetime m_lastSince;
    int m_lastSize;

};

CTimesNSales::CTimesNSales(CSymbol &pSymbol):
    m_symbol(pSymbol) {
    PrintFormat("Initializing Times & Sales for symbol : %s", m_symbol.name());
}

CTimesNSales::~CTimesNSales(void) {

}

bool CTimesNSales::retrieveSince(datetime since, int ticksCount = 1000) {
    ArrayFree(trades);
    CDatetime sinceWrapper(since);
    MqlTick ticks[];
    int copied = CopyTicks(m_symbol.name(), ticks, COPY_TICKS_TRADE, sinceWrapper.timestamp(), ticksCount);
    if (copied > 0) {
        if (m_lastSince == since && ArraySize(ticks) == m_lastSize) {
            return false;
        } else if (m_lastSince == since && ArraySize(ticks) > m_lastSize) {
            ArrayCopy(trades, ticks, 0, m_lastSize);
        } else {
            ArrayCopy(trades, ticks);
        }
        m_lastSince = since;
        m_lastSize = ArraySize(ticks);
        ArrayFree(ticks);
        return true;
    }
    return false;
}