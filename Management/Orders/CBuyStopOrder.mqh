#include <TapeReading/Management/Orders/COrderBase.mqh>
#include <TapeReading/Core/Constants.mqh>

class CBuyStopOrder : public COrderBase
{
public:
    CBuyStopOrder(const CSymbol &pSymbol, IOrderListener *pListener = NULL);
    bool operation(const double pVolume, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
};

CBuyStopOrder::CBuyStopOrder(const CSymbol &pSymbol, IOrderListener *pListener = NULL) :
    COrderBase(pSymbol, "BUY_STOP", pListener) {

}

bool CBuyStopOrder::operation(const double pVolume, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    double ask = SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK);
    if (pPrice < ask) {
        double bid = SymbolInfoDouble(m_symbol.name(), SYMBOL_BID);
        ReportAction(m_symbol, REPORT_INVALID_PRICE_ERROR, StringFormat("price: %.3f, bid: %.3f, ask: %.3f", pPrice, bid, ask));
        return(false);
    }
    return m_trade.BuyStop(pVolume, pPrice, m_symbol.name(), pStopLoss, pTakeProfit, ORDER_TIME_GTC, 0, pComment);
}
