#include <TapeReading/Core/Enums.mqh>

class CAggression
{
public:
    CAggression();
    CAggression(const CAggression &pAggression);
    CAggression(double pPrice, double pVolume, AGGRESSION_TYPE pType, datetime pDatetime);
    double price() const;
    double volume() const;
    AGGRESSION_TYPE type() const;
    string stype() const;
    bool empty() const;
    datetime time() const;

private:
    datetime m_datetime;
    double m_price;
    double m_volume;
    AGGRESSION_TYPE m_type;

};

CAggression::CAggression() :
        m_price(WRONG_VALUE), m_datetime(0), m_type(UNDEFINED) {
}

CAggression::CAggression(double pPrice, double pVolume, AGGRESSION_TYPE pType, datetime pDatetime) :
        m_price(pPrice), m_volume(pVolume), m_type(pType), m_datetime(pDatetime) {
}

CAggression::CAggression(const CAggression &pAggression) {
    m_price = pAggression.m_price;
    m_volume = pAggression.m_volume;
    m_type = pAggression.m_type;
    m_datetime = pAggression.m_datetime;
}

double CAggression::price() const {
    return m_price;
}

double CAggression::volume() const {
    return m_volume;
}

AGGRESSION_TYPE CAggression::type() const {
    return m_type;
}

string CAggression::stype() const {
    if (m_type == SELL_AGGRESSION)
        return "SELL";
    else if (m_type == BUY_AGGRESSION)
        return "BUY";
    return "UNDEFINED";
}

bool CAggression::empty() const {
    return m_price == WRONG_VALUE;
}

datetime CAggression::time() const {
    return m_datetime;
}