#include <TapeReading/Core/CSymbol.mqh>

class CAccumulation
{
public:
    CAccumulation(const CSymbol &pSymbol);
    CAccumulation(const CAccumulation &pAccumulation);
    void set(double pPrice, double pVolume, double pLow, double pHigh, int pIndexLow, int pIndexHigh);
    ~CAccumulation() {};

    bool spreadAbove();
    bool spreadBelow();
    bool spreadIn();
    double price();  // Price with highest accumulation
    double volume(); // Volume of price with highest accumulation
    double low();    // Lowest price inside the zone of accumulation
    double high();   // Highest price inside the zone of accumulation
    int depth();     // Depth of the accumulation zone

private:
    const CSymbol m_symbol;

    double m_price, m_volume;
    double m_low, m_high;
    int m_indexLow, m_indexHigh;
};

CAccumulation::CAccumulation(const CSymbol &pSymbol) :
        m_symbol(pSymbol) {

}

void CAccumulation::set(double pPrice, double pVolume, double pLow, double pHigh, int pIndexLow,  int pIndexHigh) {
    m_price= pPrice;
    m_volume= pVolume;
    m_low= pLow;
    m_high= pHigh;
    m_indexLow= pIndexLow;
    m_indexHigh= pIndexHigh;
}

CAccumulation::CAccumulation(const CAccumulation &pAccumulation):
     m_symbol(pAccumulation.m_symbol) {
    m_price     = pAccumulation.m_price;
    m_volume    = pAccumulation.m_volume;
    m_low       = pAccumulation.m_low;
    m_high      = pAccumulation.m_high;
    m_indexLow  = pAccumulation.m_indexLow;
    m_indexHigh = pAccumulation.m_indexHigh;
}



double CAccumulation::price() {
    return m_price;
}

double CAccumulation::volume() {
    return m_volume;
}

double CAccumulation::low() {
    return m_low;
}

double CAccumulation::high() {
    return m_high;
}

int CAccumulation::depth() {
    return MathMax(0, m_indexLow - m_indexHigh + 1);
}

bool CAccumulation::spreadIn() {
    double ask = SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK);
    double bid = SymbolInfoDouble(m_symbol.name(), SYMBOL_BID);
    return bid >= m_low && ask <= m_high;
}

bool CAccumulation::spreadAbove() {
    double bid = SymbolInfoDouble(m_symbol.name(), SYMBOL_BID);
    return bid > m_high;
}

bool CAccumulation::spreadBelow() {
    double ask = SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK);
    return ask < m_low;
}