#include <TapeReading/Utils/MathUtils.mqh>

#include <Math/Fuzzy/mamdanifuzzysystem.mqh>

/*------------------------------------------------------------------
Fuzzy Inference System to compute flow strength by taking into
account the following parameters: market imbalance and market
aggressiveness.
*/
class CFlowDirectionFIS
{
public:
    CFlowDirectionFIS(double pAskBidImbalance, double pVolumeOrderImbalance);
    ~CFlowDirectionFIS();
    double evaluate();

private:
    void setInputs();
    void setOutputs();
    void setRules();

    double m_askBidImbalance, m_volumeOrderImbalance;
    CFuzzyVariable *fv_askBidImbalance, *fv_volumeOrderImbalance;
    CFuzzyVariable *fv_action;
    CMamdaniFuzzySystem *m_fs;
    CMamdaniFuzzyRule *m_rule0, *m_rule1, *m_rule2, *m_rule3, *m_rule4;

};

CFlowDirectionFIS::CFlowDirectionFIS(double pAskBidImbalance, double pVolumeOrderImbalance):
    m_askBidImbalance(pAskBidImbalance), m_volumeOrderImbalance(pVolumeOrderImbalance) {
    m_fs = new CMamdaniFuzzySystem();
    setInputs();
    setOutputs();
    setRules();
}

CFlowDirectionFIS::~CFlowDirectionFIS() {
    if (CheckPointer(m_fs) != POINTER_INVALID) delete(m_fs);
}

void CFlowDirectionFIS::setInputs() {
    fv_askBidImbalance = new CFuzzyVariable("abi", -1.0, 1.0);
    fv_askBidImbalance.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(-1.0, -1.0, 0.0)));
    fv_askBidImbalance.Terms().Add(new CFuzzyTerm("average", new CTriangularMembershipFunction(-1.0, 0.0, 1.0)));
    fv_askBidImbalance.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(0.0, 1.0, 1.0)));
    m_fs.Input().Add(fv_askBidImbalance);

    fv_volumeOrderImbalance = new CFuzzyVariable("voi", -1.0, 1.0);
    fv_volumeOrderImbalance.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(-1.0, -1.0, 1.0)));
    fv_volumeOrderImbalance.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(-1.0, 1.0, 1.0)));
    m_fs.Input().Add(fv_volumeOrderImbalance);
}

void CFlowDirectionFIS::setOutputs() {
    fv_action = new CFuzzyVariable("action", -1.0, 1.0);
    fv_action.Terms().Add(new CFuzzyTerm("short", new CTriangularMembershipFunction(-1.0, -1.0, 0.0)));
    fv_action.Terms().Add(new CFuzzyTerm("do_nothing", new CTriangularMembershipFunction(-1.0, 0.0, 1.0)));
    fv_action.Terms().Add(new CFuzzyTerm("long", new CTriangularMembershipFunction(0.0, 1.0, 1.0)));
    m_fs.Output().Add(fv_action);
}

void CFlowDirectionFIS::setRules() {
    m_rule0 = m_fs.ParseRule("if (abi is low) and (voi is low) then (action is short)");
    m_rule1 = m_fs.ParseRule("if (abi is high) and (voi is high) then (action is long)");
    m_rule2 = m_fs.ParseRule("if (abi is average) then (action is do_nothing)");
    m_rule3 = m_fs.ParseRule("if (abi is low) and (voi is high) then (action is do_nothing)");
    m_rule4 = m_fs.ParseRule("if (abi is high) and (voi is low) then (action is do_nothing)");

    m_fs.Rules().Add(m_rule0);
    m_fs.Rules().Add(m_rule1);
    m_fs.Rules().Add(m_rule2);
    m_fs.Rules().Add(m_rule3);
    m_fs.Rules().Add(m_rule4);
}

double CFlowDirectionFIS::evaluate() {
    // Inputs
    CList *in = new CList;
    CDictionary_Obj_Double *p_od_abi = new CDictionary_Obj_Double;
    CDictionary_Obj_Double *p_od_voi = new CDictionary_Obj_Double;
    p_od_abi.SetAll(fv_askBidImbalance, m_askBidImbalance);
    p_od_voi.SetAll(fv_volumeOrderImbalance, m_volumeOrderImbalance);
    in.Add(p_od_abi);
    in.Add(p_od_voi);

    // Result
    CList *result;
    CDictionary_Obj_Double *out_fis;
    result = m_fs.Calculate(in);
    out_fis = result.GetNodeAtIndex(0);
    double value = ScaleValue(out_fis.Value(), -0.66, 0.66, -1.0, 1.0);

    // Deleting pointers
    if (CheckPointer(in) != POINTER_INVALID) delete(in);
    if (CheckPointer(result) != POINTER_INVALID) delete(result);
    if (CheckPointer(p_od_abi) != POINTER_INVALID) delete(p_od_abi);
    if (CheckPointer(p_od_voi) != POINTER_INVALID) delete(p_od_voi);
    if (CheckPointer(out_fis) != POINTER_INVALID) delete(out_fis);
    return value;
}
