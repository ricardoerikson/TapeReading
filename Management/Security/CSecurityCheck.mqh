#include <TapeReading/Management/Security/SecurityCheckConstants.mqh>

class CSecurityCheck {

public:
    CSecurityCheck(const string pName);
    bool pass();
    virtual bool check() = 0;
    virtual void report() const = 0;
    void enable(bool pEnable);
    string name() const;

protected:
    const string m_name;
    bool m_enabled;

};

CSecurityCheck::CSecurityCheck(const string pName) :
    m_name(pName), m_enabled(false) {
}

void CSecurityCheck::enable(bool pEnable) {
    m_enabled = pEnable;
}

bool CSecurityCheck::pass() {
    if (m_enabled == false)
        return(true);
    if (!check()) {
        report();
        return(false);
    }
    return(true);
}

string CSecurityCheck::name() const {
    return m_name;
}