#include <TapeReading/Utils/MathUtils.mqh>

#include <Math/Fuzzy/mamdanifuzzysystem.mqh>


/*------------------------------------------------------------------
Fuzzy Inference System for price volume. This inference system
analyzes the volume given the mean of a market side.
*/
class CEndOfQueueFIS
{
public:
    CEndOfQueueFIS(double pMinVolume, double pMaxVolume, double pPriceVolume);
    ~CEndOfQueueFIS();
    double evaluate();

private:
    void setInputs();
    void setOutputs();
    void setRules();
    CFuzzyVariable          *fvVolume;          // Fuzzy variable for price volume
    CFuzzyVariable          *fvContracts;          // Fuzzy variables for fvContracts
    CMamdaniFuzzySystem     *mFs;               // Mamdani Fuzzy Inference System
    CMamdaniFuzzyRule       *rule0, *rule1;
    const double m_minVolume, m_maxVolume, m_intendedVolume, MIN_INFERENCED_VALUE, MAX_INFERENCED_VALUE;
    double m_priceVolume, m_minInferencedValue, m_maxInferencedValue;
};

CEndOfQueueFIS::CEndOfQueueFIS(double pMinVolume, double pMaxVolume, double pPriceVolume) :
        m_minVolume(MathMax(0.0, pMinVolume)), m_maxVolume(MathMax(0.0, pMaxVolume)),
        MIN_INFERENCED_VALUE(1.0/3.0), MAX_INFERENCED_VALUE(2.0/3.0) {
    m_priceVolume = MathMax(0.0, MathMin(m_maxVolume, pPriceVolume));
    mFs = new CMamdaniFuzzySystem();
    setInputs();
    setOutputs();
    setRules();
}

CEndOfQueueFIS::~CEndOfQueueFIS() {
    if (CheckPointer(mFs) != POINTER_INVALID) delete(mFs);
}

void CEndOfQueueFIS::setInputs() {
    fvVolume = new CFuzzyVariable("volume", 0.0, m_maxVolume);
    fvVolume.Terms().Add(new CFuzzyTerm("low", new CTrapezoidMembershipFunction(0.0, 0.0, m_minVolume, m_maxVolume)));
    fvVolume.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(m_minVolume, m_maxVolume, m_maxVolume)));
    mFs.Input().Add(fvVolume);
}

void CEndOfQueueFIS::setOutputs() {
    fvContracts = new CFuzzyVariable("contracts", 0.0, 1.0);
    fvContracts.Terms().Add(new CFuzzyTerm("few", new CTriangularMembershipFunction(0.0, 0.0, 1.0)));
    fvContracts.Terms().Add(new CFuzzyTerm("many", new CTriangularMembershipFunction(0.0, 1.0, 1.0)));
    mFs.Output().Add(fvContracts);
}

void CEndOfQueueFIS::setRules() {
    rule0 = mFs.ParseRule("if (volume is not high) then (contracts is few)"); // It is not the end of the queue yet.
    rule1 = mFs.ParseRule("if (volume is high) then (contracts is many)");
    mFs.Rules().Add(rule0);
    mFs.Rules().Add(rule1);
}

double CEndOfQueueFIS::evaluate() {
    CList *in = new CList;
    CDictionary_Obj_Double *p_od_volume = new CDictionary_Obj_Double;

    p_od_volume.SetAll(fvVolume, m_priceVolume);
    in.Add(p_od_volume);
    CList* result;
    CDictionary_Obj_Double *p_od_fs;
    result = mFs.Calculate(in);
    p_od_fs = result.GetNodeAtIndex(0);
    double value = ScaleValue(p_od_fs.Value(), MIN_INFERENCED_VALUE, MAX_INFERENCED_VALUE, 0.0, 1.0);
    if (CheckPointer(in) != POINTER_INVALID) delete(in);
    if (CheckPointer(result) != POINTER_INVALID) delete(result);
    if (CheckPointer(p_od_volume) != POINTER_INVALID) delete(p_od_volume);
    if (CheckPointer(p_od_fs) != POINTER_INVALID) delete(p_od_fs);
    return value;
}
