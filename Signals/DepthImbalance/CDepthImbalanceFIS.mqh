#include <TapeReading/Utils/MathUtils.mqh>

#include <Math/Fuzzy/mamdanifuzzysystem.mqh>


/*------------------------------------------------------------------
Fuzzy Inference System for Depth Imbalance inference.
*/
class CDepthImbalanceFIS
{
public:
    CDepthImbalanceFIS(double pFirstLevel, double pSecondLevel);
    ~CDepthImbalanceFIS();
    double evaluate();

private:
    void setInputs();
    void setOutputs();
    void setRules();

    CFuzzyVariable          *fvFirstLevelPressure, *fvSecondLevelPressure; // Fuzzy variable for price accumulative levels
    CFuzzyVariable          *fvOverallPressure;                            // Fuzzy variables for market pressure
    CMamdaniFuzzySystem     *mFs;                                          // Mamdani Fuzzy Inference System
    CMamdaniFuzzyRule       *rule0, *rule1;

    const double m_firstLevel, m_secondLevel;
    const double MIN_INFERENCED_VALUE, MAX_INFERENCED_VALUE;
};

CDepthImbalanceFIS::CDepthImbalanceFIS(double pFirstLevel, double pSecondLevel) :
        m_firstLevel(pFirstLevel),
        m_secondLevel(pSecondLevel),
        MIN_INFERENCED_VALUE(-1.0/3.0),
        MAX_INFERENCED_VALUE(1.0/3.0) {
    mFs = new CMamdaniFuzzySystem();
    setInputs();
    setOutputs();
    setRules();
}

CDepthImbalanceFIS::~CDepthImbalanceFIS() {
    if (CheckPointer(mFs) != POINTER_INVALID) delete(mFs);
}

void CDepthImbalanceFIS::setInputs() {
    fvFirstLevelPressure = new CFuzzyVariable("fl_pressure", -1.0, 1.0);
    fvFirstLevelPressure.Terms().Add(new CFuzzyTerm("down", new CTriangularMembershipFunction(-1.0, -1.0, 1.0)));
    fvFirstLevelPressure.Terms().Add(new CFuzzyTerm("up", new CTriangularMembershipFunction(-1.0, 1.0, 1.0)));
    mFs.Input().Add(fvFirstLevelPressure);

    fvSecondLevelPressure = new CFuzzyVariable("sl_pressure", -1.0, 1.0);
    fvSecondLevelPressure.Terms().Add(new CFuzzyTerm("down", new CTriangularMembershipFunction(-1.0, -1.0, 1.0)));
    fvSecondLevelPressure.Terms().Add(new CFuzzyTerm("up", new CTriangularMembershipFunction(-1.0, 1.0, 1.0)));
    mFs.Input().Add(fvSecondLevelPressure);
}

void CDepthImbalanceFIS::setOutputs() {
    fvOverallPressure = new CFuzzyVariable("overall_pressure", -1.0, 1.0);
    fvOverallPressure.Terms().Add(new CFuzzyTerm("down", new CTriangularMembershipFunction(-1.0, -1.0, 1.0)));
    fvOverallPressure.Terms().Add(new CFuzzyTerm("up", new CTriangularMembershipFunction(-1.0, 1.0, 1.0)));
    mFs.Output().Add(fvOverallPressure);
}

void CDepthImbalanceFIS::setRules() {
    rule0 = mFs.ParseRule("if (fl_pressure is up) and (sl_pressure is up) then (overall_pressure is up)");
    rule1 = mFs.ParseRule("if (fl_pressure is down) and (sl_pressure is down) then (overall_pressure is down)");
    mFs.Rules().Add(rule0);
    mFs.Rules().Add(rule1);
}

double CDepthImbalanceFIS::evaluate() {
    CList *in = new CList;
    CDictionary_Obj_Double *p_od_firstLevel = new CDictionary_Obj_Double;
    CDictionary_Obj_Double *p_od_secondLevel = new CDictionary_Obj_Double;

    p_od_firstLevel.SetAll(fvFirstLevelPressure, m_firstLevel);
    p_od_secondLevel.SetAll(fvSecondLevelPressure, m_secondLevel);
    in.Add(p_od_firstLevel);
    in.Add(p_od_secondLevel);

    CList* result;
    CDictionary_Obj_Double *p_od_fs;

    result = mFs.Calculate(in);
    p_od_fs = result.GetNodeAtIndex(0);
    double value = ScaleValue(p_od_fs.Value(), MIN_INFERENCED_VALUE, MAX_INFERENCED_VALUE, -1.0, 1.0);
    if (CheckPointer(in) != POINTER_INVALID) delete(in);
    if (CheckPointer(result) != POINTER_INVALID) delete(result);
    if (CheckPointer(p_od_firstLevel) != POINTER_INVALID) delete(p_od_firstLevel);
    if (CheckPointer(p_od_secondLevel) != POINTER_INVALID) delete(p_od_secondLevel);
    if (CheckPointer(p_od_fs) != POINTER_INVALID) delete(p_od_fs);
    return value;
}
