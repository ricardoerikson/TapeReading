#include <TapeReading/PChart/CPChart.mqh>

#include <TapeReading/Patterns/Observer/Observable.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>
#include <TapeReading/Core/CSymbol.mqh>
#include <TapeReading/Core/Enums.mqh>

class CTimesNSales;

class CFrontRunningObservable : public Observable<IScalpingObserver>
{
public:
    CFrontRunningObservable(CSymbol &pSymbol, CPChart *pPChart);
    ~CFrontRunningObservable();
    CAggression lastAggression() const;
    string name() const;
    void notifyObservers();

private:
    /*------------------------------------------------------------------
    Strategies
    */
    bool strategyBuy() const;
    bool strategySell() const;

    CSymbol m_symbol;
    CPChart *m_pChart;

    int m_barsCount;
    double m_lastFRAsk;
    double m_lastFRBid;
};

CFrontRunningObservable::CFrontRunningObservable(CSymbol &pSymbol, CPChart *pPChart) :
        m_symbol(pSymbol), m_lastFRAsk(0), m_barsCount(-1) {
    m_pChart = pPChart;
    m_lastFRBid = DBL_MAX;
}

CFrontRunningObservable::~CFrontRunningObservable() {
}

string CFrontRunningObservable::name() const {
    return "Front Running Observable";
}

bool CFrontRunningObservable::strategyBuy() const {
    if (ArraySize(m_pChart.bars) == 0)
        return false;
    if (m_pChart.bars[0].direction() == BAR_UP && m_pChart.bars[0].buySlope() >= 0) {
        MqlTick tick;
        SymbolInfoTick(m_symbol.name(), tick);
        if (m_pChart.buy().slope() > 0 && /*m_pChart.sell().slope() >= 0 &&*/ tick.ask > m_pChart.bars[0].close()) {
            return true;
        }
    }
    return false;
}

bool CFrontRunningObservable::strategySell() const {
    if (ArraySize(m_pChart.bars) == 0)
        return false;
    if (m_pChart.bars[0].direction() == BAR_DOWN && m_pChart.bars[0].sellSlope() <= 0) {
        MqlTick tick;
        SymbolInfoTick(m_symbol.name(), tick);
        if (m_pChart.sell().slope() < 0 && /*m_pChart.buy().slope() <= 0 &&*/ tick.bid < m_pChart.bars[0].close())
            return true;
    }
    return false;
}

void CFrontRunningObservable::notifyObservers() {
    /*------------------------------------------------------------------
    Init last Front Running bid and Front Running ask if new bar
    */
    if (m_barsCount != ArraySize(m_pChart.bars)) {
        m_barsCount = ArraySize(m_pChart.bars);
        m_lastFRAsk = 0;
        m_lastFRBid = DBL_MAX;
    }
    if (m_pChart.buy().count() == 0) {
        m_lastFRAsk = 0;
    }
    if (m_pChart.sell().count() == 0) {
        m_lastFRBid = DBL_MAX;
    }
    CAggression lastAggression = lastAggression();
    if (lastAggression.empty())
        return;

    bool goLong = strategyBuy();
    bool goShort = strategySell();

    /*------------------------------------------------------------------
    If both are true then do nothing
    */
    if (goLong && goShort)
        return;

    if(goLong && lastAggression.type() == BUY_AGGRESSION && lastAggression.price() > m_lastFRAsk) {
        for(int i = 0; i < observersCount(); ++i)
            observer(i).onFrontRunningBuy();
        m_lastFRAsk = lastAggression.price();
        return;
    }

    if (goShort && lastAggression.type() == SELL_AGGRESSION && lastAggression.price() < m_lastFRBid) {
        for (int i = 0; i < observersCount(); ++i)
            observer(i).onFrontRunningSell();
        m_lastFRBid = lastAggression.price();
    }

}

CAggression CFrontRunningObservable::lastAggression() const {
    CAggression buyAggression = m_pChart.buy().last();
    CAggression sellAggression = m_pChart.sell().last();
    if (!buyAggression.empty() && buyAggression.time() > sellAggression.time())
        return buyAggression;
    else if (!sellAggression.empty() && sellAggression.time() > buyAggression.time())
        return sellAggression;
    CAggression empty;
    return empty;
}
