#include <TapeReading/Management/Orders/COrderBase.mqh>
#include <TapeReading/Core/Constants.mqh>

class CSellLimitOrder : public COrderBase
{
public:
    CSellLimitOrder(const CSymbol &pSymbol, IOrderListener *pListener = NULL);
    bool operation(const double pVolume, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
};

CSellLimitOrder::CSellLimitOrder(const CSymbol &pSymbol, IOrderListener *pListener = NULL) :
    COrderBase(pSymbol, "SELL_LIMIT", pListener) {

}

bool CSellLimitOrder::operation(const double pVolume, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    double ask = SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK);
    if (pPrice >= ask) {
        double bid = SymbolInfoDouble(m_symbol.name(), SYMBOL_BID);
        ReportAction(m_symbol, REPORT_INVALID_PRICE_ERROR, StringFormat("price: %.3f, bid: %.3f, ask: %.3f", pPrice, bid, ask));
        return(false);
    }
    return m_trade.SellLimit(pVolume, pPrice, m_symbol.name(), pStopLoss, pTakeProfit, ORDER_TIME_GTC, 0, pComment);
}
