#include <TapeReading/Core/Enums.mqh>

class CBarData
{
public:
    CBarData(){};
    CBarData(double pLow, double pHigh, double pOpen, double pClose,
        double pBuyMean, double pSellMean,
        double pBuySlope, double pSellSlope,
        double pBuyVolume, double pSellVolume,
        datetime pOpenTime, datetime pCloseTime);
    CBarData(const CBarData &pObject);

    BAR_DIRECTION direction() const;
    datetime openTime() const;
    datetime closeTime() const;
    double low() const;
    double high() const;
    double open() const;
    double close() const;
    double buyMean() const;
    double sellMean() const;
    double buySlope() const;
    double sellSlope() const;
    double buyVolume() const;
    double sellVolume() const;
    double imbalance() const;
    string description() const;

private:

    datetime m_openTime;
    datetime m_closeTime;

    double m_low;
    double m_high;
    double m_open;
    double m_close;

    /*------------------------------------------------------------------
    Mean for big aggressions inside the bar
    */
    double m_buyMean;
    double m_sellMean;

    /*------------------------------------------------------------------
    Slope for big aggressions
    */
    double m_buySlope;
    double m_sellSlope;

    /*------------------------------------------------------------------
    Volume for big aggressions
    */
    double m_buyVolume;
    double m_sellVolume;

};

CBarData::CBarData(double pLow, double pHigh, double pOpen, double pClose,
    double pBuyMean, double pSellMean,
    double pBuySlope, double pSellSlope,
    double pBuyVolume, double pSellVolume,
    datetime pOpenTime, datetime pCloseTime) :
        m_low(pLow),
        m_high(pHigh),
        m_open(pOpen),
        m_close(pClose),
        m_buyMean(pBuyMean),
        m_sellMean(pSellMean),
        m_buySlope(pBuySlope),
        m_sellSlope(pSellSlope),
        m_buyVolume(pBuyVolume),
        m_sellVolume(pSellVolume),
        m_openTime(pOpenTime),
        m_closeTime(pCloseTime) {
}

CBarData::CBarData(const CBarData &pObject) {
    m_low = pObject.m_low;
    m_high = pObject.m_high;
    m_open = pObject.m_open;
    m_close = pObject.m_close;
    m_buyMean = pObject.m_buyMean;
    m_sellMean = pObject.m_sellMean;
    m_buySlope = pObject.m_buySlope;
    m_sellSlope = pObject.m_sellSlope;
}

/*------------------------------------------------------------------
Returns the direction of the bar.

Returns:
    true if bar is positive and false if bar is negative
*/
BAR_DIRECTION CBarData::direction() const {
    return (m_close > m_open) ? BAR_UP : BAR_DOWN;
}

double CBarData::low() const {
    return m_low;
}

double CBarData::high() const {
    return m_high;
}

double CBarData::open() const {
    return m_open;
}

double CBarData::close() const {
    return m_close;
}

double CBarData::buyMean() const {
    return m_buyMean;
}

double CBarData::sellMean() const {
    return m_sellMean;
}

double CBarData::buySlope() const {
    return m_buySlope;
}

double CBarData::sellSlope() const {
    return m_sellSlope;
}

double CBarData::buyVolume() const {
    return m_buyVolume;
}

double CBarData::sellVolume() const {
    return m_sellVolume;
}

datetime CBarData::openTime() const {
    return m_openTime;
}

datetime CBarData::closeTime() const {
    return m_closeTime;
}

double CBarData::imbalance() const {
    double total = m_buyVolume + m_sellVolume;
    if (total == 0)
        return 0.0;
    return (m_buyVolume - m_sellVolume) / total;
}

string CBarData::description() const {

    string tpl =  "\n#[openTime: %s, closeTime: %s]";
           tpl += "\n#[open: %.3f, close: %.3f, low: %.3f, high: %.3f]";
           tpl += "\n#info:";
           tpl += "\n      - direction: %s";
           tpl += "\n      - buyMean:   %04.3f, sellMean:   %04.3f";
           tpl += "\n      - buySlope:  %04.3f, sellSlope:  %04.3f";
           tpl += "\n      - buyVolume: %08.0f, sellVolume: %08.0f,     imbalance: %01.3f";
    return StringFormat(tpl, TimeToString(m_openTime, TIME_SECONDS), TimeToString(m_closeTime, TIME_SECONDS),
        m_open, m_close, m_low, m_high,
        (direction() == BAR_UP)? "UP": "DOWN",
        m_buyMean, m_sellMean,
        m_buySlope, m_sellSlope,
        m_buyVolume, m_sellVolume, imbalance());
}