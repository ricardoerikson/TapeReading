#include <TapeReading/Management/Security/CSecurityCheck.mqh>
#include <TapeReading/Management/Trade/TradeReport.mqh>

class CMaxEquityCheck : public CSecurityCheck
{
public:
    CMaxEquityCheck(double pEquity, double pPercent);
    bool check();
    void report() const;

private:
    const double m_equity;
    const double m_percent;
};

CMaxEquityCheck::CMaxEquityCheck(double pEquity, double pPercent) :
    CSecurityCheck(MAX_EQUITY_CHECK), m_equity(pEquity), m_percent(pPercent) {
}

bool CMaxEquityCheck::check() {
    double equity = AccountInfoDouble(ACCOUNT_EQUITY);
    if (equity > m_equity * m_percent)
        return(false);
    return true;
}

void CMaxEquityCheck::report() const {
    double equity = AccountInfoDouble(ACCOUNT_EQUITY);
    double maxEquityAllowed = m_equity * m_percent;
    ReportSecurityCheck(m_name, StringFormat("current equity: %.2f, max equity allowed: %.2f", equity, maxEquityAllowed));
}