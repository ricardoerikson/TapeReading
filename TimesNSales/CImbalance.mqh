class CImbalance
{
public:
    CImbalance(const CImbalance &object);
    CImbalance(int percent = 200, int minDiff = 25);
    ~CImbalance();
    double imbalance();
    void addBuy(double volume);
    void addSell(double volume);
    double volume();
    double trades();
    double totalVolume();
    double totalTrades();

private:
    double volumeDiff();
    double mBuyVolume, mSellVolume;
    double mBuyTrades, mSellTrades;
    int mPercent, mMinDiff;

};

CImbalance::CImbalance(const CImbalance &object) {
    mBuyVolume = object.mBuyVolume;
    mSellVolume = object.mSellVolume;
    mBuyTrades = object.mBuyTrades;
    mSellTrades = object.mSellTrades;
    mPercent = object.mPercent;
    mMinDiff = object.mMinDiff;
}

CImbalance::CImbalance(int percent = 200, int minDiff = 25):
    mBuyVolume(0), mSellVolume(0), mBuyTrades(0), mSellTrades(0),
    mPercent(percent), mMinDiff(minDiff) {
}

CImbalance::~CImbalance() {
}

void CImbalance::addBuy(double value) {
    mBuyVolume += MathMax(0, value);
    mBuyTrades += 1;
}

void CImbalance::addSell(double value) {
    mSellVolume += MathMax(0, value);
    mSellTrades += 1;
}

double CImbalance::totalVolume() {
    return mBuyVolume + mSellVolume;
}

double CImbalance::totalTrades() {
    return mBuyTrades + mSellTrades;
}

double CImbalance::volume() {
    if (volumeDiff() < mMinDiff)
        return 0.0;
    double min = MathMin(mBuyVolume, mSellVolume);
    if (min == 0)
        return (mBuyVolume == 0) ? -1.0 : 1.0;
    double max = MathMax(mBuyVolume, mSellVolume);
    if ((max / min) >= (double) MathMax(mPercent, 100) / (MathMin(mPercent, 100)))
        return NormalizeDouble((mBuyVolume - mSellVolume) / (mBuyVolume + mSellVolume), 2);
    return 0.0;
;
}

double CImbalance::trades() {
    if (totalTrades() <= 0)
        return 0.0;
    return NormalizeDouble((mBuyTrades - mSellTrades) / (mBuyTrades + mSellTrades), 2);
}

double CImbalance::volumeDiff() {
    return MathAbs(mBuyVolume - mSellVolume);
}