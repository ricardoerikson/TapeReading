#include <TapeReading/Management/Security/CSecurityCheck.mqh>
#include <TapeReading/Management/Trade/TradeReport.mqh>

class CLockoutCheck : public CSecurityCheck
{
public:
    CLockoutCheck(double pLockout, double pInitialEquity);
    bool check();
    void report() const;

private:
    const double m_lockout;
    const double m_initialEquity;

};

CLockoutCheck::CLockoutCheck(double pLockout, double pInitialEquity) :
    CSecurityCheck(LOCKOUT_CHECK), m_lockout(pLockout), m_initialEquity(pInitialEquity) {
}

bool CLockoutCheck::check() {
    double equity = AccountInfoDouble(ACCOUNT_EQUITY);
    if (equity < (m_initialEquity - m_lockout))
        return(false);
    return(true);
}

void CLockoutCheck::report() const {
    double equity = AccountInfoDouble(ACCOUNT_EQUITY);
    ReportSecurityCheck(m_name, StringFormat("current equity: %.2f, min equity allowed: %.2f", equity, (m_initialEquity - m_lockout)));
}