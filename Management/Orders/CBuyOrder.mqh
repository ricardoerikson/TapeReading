#include <TapeReading/Management/Orders/COrderBase.mqh>

class CBuyOrder : public COrderBase
{
public:
    CBuyOrder(const CSymbol &pSymbol, IOrderListener *pListener = NULL);
    bool operation(const double pVolume, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
};

CBuyOrder::CBuyOrder(const CSymbol &pSymbol, IOrderListener *pListener = NULL) :
    COrderBase(pSymbol, "BUY", pListener) {

}

bool CBuyOrder::operation(const double pVolume, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    return m_trade.Buy(pVolume, m_symbol.name(), 0, pStopLoss, pTakeProfit, pComment);
}
