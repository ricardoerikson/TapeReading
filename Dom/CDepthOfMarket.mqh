#include <TapeReading/Dom/CMarketBook.mqh>
#include <TapeReading/Dom/CAsk.mqh>
#include <TapeReading/Dom/CBid.mqh>

#include <TapeReading/Core/CGenericImbalance.mqh>
#include <TapeReading/Core/CSymbol.mqh>

class CDepthOfMarket
{
public:
    CDepthOfMarket(CSymbol &pSymbol, double pMaxDeviation = 15, double pImbalanceRatio = 0, double pImbalanceDiff = 0);
    ~CDepthOfMarket();
    CAsk* ask();
    CBid* bid();
    CMarketBook* book();
    CSymbol symbol();
    void update();
    bool isAuction();
    double imbalance(int pDepth = 1, bool pIncludeFirst = true);

private:
    void setBestAskAndBidIndexes();
    bool hasBestBidIndex();

    double m_imbalanceRatio, m_imbalanceDiff;

    CMarketBook *m_marketBook;
    CAsk *m_ask;
    CBid *m_bid;
    CSymbol m_symbol;

};

CDepthOfMarket::CDepthOfMarket(CSymbol &pSymbol, double pMaxDeviation, double pImbalanceRatio, double pImbalanceDiff):
        m_symbol(pSymbol),
        m_imbalanceRatio(pImbalanceRatio),
        m_imbalanceDiff(pImbalanceDiff) {
    PrintFormat("Initializing Depth of Market (DOM) for symbol: %s", m_symbol.name());
    m_marketBook = new CMarketBook(m_symbol);
    m_ask = new CAsk(m_marketBook, m_symbol, pMaxDeviation);
    m_bid = new CBid(m_marketBook, m_symbol, pMaxDeviation);
}

CDepthOfMarket::~CDepthOfMarket() {
    if (CheckPointer(m_marketBook) != POINTER_INVALID) delete(m_marketBook);
    if (CheckPointer(m_ask) != POINTER_INVALID) delete(m_ask);
    if (CheckPointer(m_bid) != POINTER_INVALID) delete(m_bid);
    PrintFormat("Depth of Market (DOM) for symbol %s was destroyed.", m_symbol.name());
}

bool CDepthOfMarket::hasBestBidIndex() {
    bool isCurrentAskWithinBounds = ask().bestIndex() >= 0 && ask().bestIndex() < book().depth();
    if (!isCurrentAskWithinBounds)
        return false;
    bool isCurrentAskInAskSide = m_marketBook.book[ask().bestIndex()].type == BOOK_TYPE_SELL || m_marketBook.book[ask().bestIndex()].type == BOOK_TYPE_SELL_MARKET;
    if (!isCurrentAskWithinBounds && isCurrentAskInAskSide)
        return false;
    int bestBidIndex = ask().bestIndex() + 1;
    bool isCurrentBidWithinBounds = bestBidIndex >= 0 && bestBidIndex < book().depth();
    if (!isCurrentBidWithinBounds)
        return false;
    bool isCurrentBidInBidSide = (m_marketBook.book[bestBidIndex].type == BOOK_TYPE_BUY || m_marketBook.book[bestBidIndex].type == BOOK_TYPE_BUY_MARKET);
    if (isCurrentBidWithinBounds && isCurrentBidInBidSide)
        return true;
    return false;
}

void CDepthOfMarket::setBestAskAndBidIndexes() {
    if (!hasBestBidIndex()) {
        bid().bestIndex(-1);
        ask().bestIndex(-1);
        int bookSize = ArraySize(m_marketBook.book);
        if (bookSize == 0)
            return;
        ask().bestIndex(bookSize - 1);
        for (int i = 0; i < bookSize; ++i) {
            if (m_marketBook.book[i].type == BOOK_TYPE_BUY || m_marketBook.book[i].type == BOOK_TYPE_BUY_MARKET) {
                ask().bestIndex(i - 1);
                bid().bestIndex(i);
                break;
            }
        }
    }
}

CAsk* CDepthOfMarket::ask() {
    return m_ask;
}

CBid* CDepthOfMarket::bid() {
    return m_bid;
}

CMarketBook* CDepthOfMarket::book() {
    return m_marketBook;
}

void CDepthOfMarket::update() {
    m_marketBook.update();
    setBestAskAndBidIndexes();
    int bookSize = book().depth();
    if (bid().exists())
        bid().lastIndex(bookSize - 1);
    if (ask().exists())
        ask().lastIndex(0);
}

bool CDepthOfMarket::isAuction() {
    double ask = SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK);
    double bid = SymbolInfoDouble(m_symbol.name(), SYMBOL_BID);
    return (bid > ask);
}

CSymbol CDepthOfMarket::symbol() {
    return m_symbol;
}

double CDepthOfMarket::imbalance(int pDepth, bool pIncludeFirst) {
    if (pDepth == 1 && pIncludeFirst == false)
        return 0.0;
    double bidFirstLevel = bid().volume(0);
    double askFirstLevel = ask().volume(0);
    double bidVolume, askVolume;
    if (pDepth <= 0)
        return 0.0;
    else if (pDepth == 1) {
        bidVolume = bidFirstLevel;
        askVolume = askFirstLevel;
    } else {
        if (pIncludeFirst == true) {
            bidVolume = bid().accumulatedVolume(pDepth);
            askVolume = ask().accumulatedVolume(pDepth);
        } else {
            bidVolume = bid().accumulatedVolume(pDepth) - bidFirstLevel;
            askVolume = ask().accumulatedVolume(pDepth) - askFirstLevel;
        }
    }
    CGenericImbalance imbalanceObj(bidVolume, askVolume, m_imbalanceDiff, m_imbalanceRatio);
    return imbalanceObj.value();
}