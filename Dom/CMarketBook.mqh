#include <TapeReading/Core/CSymbol.mqh>

class CMarketBook
{
public:
    CMarketBook(const CSymbol &pSymbol);
    ~CMarketBook();
    int depth();
    void update();
    MqlBookInfo book[];

private:
    const CSymbol m_symbol;

};

CMarketBook::CMarketBook(const CSymbol &pSymbol):
    m_symbol(pSymbol) {
    MarketBookAdd(m_symbol.name());
}

CMarketBook::~CMarketBook() {
    MarketBookRelease(m_symbol.name());
}

void CMarketBook::update() {
    MarketBookGet(m_symbol.name(), book);
}

int CMarketBook::depth() {
    return ArraySize(book);
}