#include <TapeReading/TimesNSales/CTimesNSales.mqh>
#include <TapeReading/PChart/CPriceLevel.mqh>
#include <TapeReading/Core/CSymbol.mqh>
#include <TapeReading/PChart/IPBarListener.mqh>
#include <TapeReading/PChart/CAccumulation.mqh>

#include <TapeReading/Utils/TickUtils.mqh>
#include <TapeReading/Utils/ArrayUtils.mqh>

class CPBar
{
public:
    CPBar(CSymbol &pSymbol, IPBarListener *pListener, CTimesNSales *pTimesNSales, int pVariations, double pOpenPrice, double pAccumulationFactor, int mTimesNSalesIndex = 0);
    ~CPBar();
    CAccumulation* accumulation();

    bool channeling(int pChannelingTime = 120) const;
    datetime openTime() const;
    double close() const;
    double open() const;
    double high() const;
    double leftover() const;
    double low() const;
    double volume() const;
    double mean() const;
    int depth() const;
    int elapsedTime() const;
    void print() const;
    void update(int index = 0);

private:
    bool belongs(double pPrice) const;
    double getPrice(int pIndex) const;
    int getIndex(double pPrice) const;
    void update(MqlTick &pTick, int pTimesNSalesIndex);
    void updateIndexes();

    CAccumulation *m_accumulation;
    CPriceLevel m_levels[];
    CSymbol m_symbol;
    CTimesNSales *m_timesNSales;
    IPBarListener *m_listener;

    datetime m_openTime, m_closeTime;
    double m_openPrice, m_closePrice, m_previousPrice, m_accumulationFactor;
    int m_timesNSalesIndex;
    int m_indexAccumPrice;          // index of the price level with highest volume in the accumulation zone
    int m_indexHigh;                // Index of the highest price level
    int m_indexLow;                 // Index of the lowest price level
    int m_variations;               // Minimum difference between current price and open price (in ticks) to open a new p bar
    double m_leftover;              // Aggression leftover in this bar. That is, the difference between buy and sell aggressions

};

CPBar::CPBar(CSymbol &pSymbol, IPBarListener *pListener, CTimesNSales *pTimesNSales, int pVariations, double pOpenPrice, double pAccumulationFactor, int pTimesNSalesIndex):
        m_symbol(pSymbol), m_variations(pVariations), m_openPrice(pOpenPrice),
        m_openTime(TimeCurrent()), m_timesNSalesIndex(pTimesNSalesIndex),
        m_accumulationFactor(pAccumulationFactor) {
    m_listener = pListener;
    m_timesNSales = pTimesNSales;
    ArrayResize(m_levels, m_variations * 2 + 1);
    m_accumulation = new CAccumulation(m_symbol);
    if (m_timesNSalesIndex > 0)
        update(m_timesNSalesIndex);
}

CPBar::~CPBar() {
    ArrayFree(m_levels);
    if (CheckPointer(m_accumulation) != POINTER_INVALID) delete(m_accumulation);
}

bool CPBar::belongs(double pPrice) const {
    int priceIndex = getIndex(pPrice);
    return priceIndex >= 0 && priceIndex <= 2*m_variations;
}

int CPBar::depth() const {
    return MathMax(0, m_indexLow - m_indexHigh + 1);
}

int CPBar::getIndex(double pPrice) const {
    return (int)(m_variations - (pPrice - m_openPrice) / m_symbol.tickSize());
}

double CPBar::getPrice(int pIndex) const {
    return (m_variations - pIndex) * m_symbol.tickSize() + m_openPrice;
}

/*------------------------------------------------------------------
Returns the highest price of the bar.

Returns:
    Highest price.
*/
double CPBar::high() const {
    return getPrice(m_indexHigh);
}

/*------------------------------------------------------------------
Returns the lowest price of the bar.

Returns:
    Lowest price.
*/
double CPBar::low() const {
    return getPrice(m_indexLow);
}

CAccumulation* CPBar::accumulation() {
    return m_accumulation;
}

/*------------------------------------------------------------------
Returns elapsed time since the bar opening time.

Returns:
    Elapsed time since bar opening time.
*/
int CPBar::elapsedTime() const {
    return (int) (TimeCurrent() - m_openTime);
}

/*------------------------------------------------------------------
Returns aggression remaining leftover of the bar.

Returns:
    Leftover of the bar.
*/
double CPBar::leftover() const {
    return m_leftover;
}

/*------------------------------------------------------------------
Returns opening time of the bar.

Returns:
    Opening time of the bar.
*/
datetime CPBar::openTime() const {
    return m_openTime;
}

/*------------------------------------------------------------------
Returns the total volume of the bar.

Returns:
    Total volume of the bar taking into account all price levels.
*/
double CPBar::volume() const {
    if (ArraySize(m_levels) == 0 || m_indexHigh == -1 || m_indexLow == -1)
        return 0;
    double volume = 0;
    for (int i = m_indexHigh; i <= m_indexLow; i++) {
        volume += m_levels[i].total();
    }
    return volume;
}

void CPBar::update(MqlTick &pTick, int pTimesNSalesIndex) {
    if (!belongs(pTick.last)) {
        m_closePrice = m_previousPrice;
        m_listener.onCloseAndOpenBar((m_previousPrice == 0.0) ? pTick.last : m_previousPrice, pTimesNSalesIndex);
        return;
    }
    int priceIndex = getIndex(pTick.last);
    if (IsBuyAggression(pTick)) {
        m_levels[priceIndex].addBuyVolume(pTick.volume);
    } else if (IsSellAggression(pTick)) {
        m_levels[priceIndex].addSellVolume(pTick.volume);
    }
    m_previousPrice = pTick.last;
}

void CPBar::updateIndexes() {
    m_indexHigh = -1;
    m_indexAccumPrice = -1;
    m_indexLow = -1;
    for (int i = 0; i < ArraySize(m_levels); i++)
        if (m_levels[i].total() != 0) {
            m_indexHigh = i;
            break;
        }
    for (int i = ArraySize(m_levels) - 1; i >= 0; i--)
        if (m_levels[i].total() != 0) {
            m_indexLow = i;
            break;
        }
    m_indexAccumPrice = m_indexLow;
    m_leftover = 0;

    double prices[];
    double values[];
    int idx = 0;
    ArrayResize(prices, depth());
    ArrayResize(values, depth());
    double pVolume = volume();
    if (pVolume == 0)
        return;
    for (int i = m_indexHigh; i <= m_indexLow; i++) {
        prices[idx] = getPrice(i);
        values[idx] = NormalizeDouble(m_levels[i].total() / pVolume, 3);
        idx++;
        m_leftover += m_levels[m_indexAccumPrice].delta();
        if (m_levels[i].total() > m_levels[m_indexAccumPrice].total())
            m_indexAccumPrice = i;
    }
    QuickSort(values, 0, ArraySize(values) - 1, prices);
    double sum = 0;
    int i;
    for (i = ArraySize(values) - 1; i > 0 ; i--) {
        sum += values[i];
        if (sum >= m_accumulationFactor)
            break;
    }
    int indexAccumLow = ArrayMinimum(prices, i);
    int indexAccumHigh = ArrayMaximum(prices, i);
    double priceAccumLow = prices[indexAccumLow];
    double priceAccumHigh = prices[indexAccumHigh];
    double volume = m_levels[m_indexAccumPrice].total();
    m_accumulation.set(getPrice(m_indexAccumPrice), volume, priceAccumLow, priceAccumHigh, getIndex(priceAccumLow), getIndex(priceAccumHigh));
}

void CPBar::update(int index) {
    int tnsSize = ArraySize(m_timesNSales.trades);
    if (tnsSize == 0)
        return;
    for(int i = index; i < ArraySize(m_timesNSales.trades); ++i) {
        MqlTick tick = m_timesNSales.trades[i];
        update(tick, i);
    }
    updateIndexes();
}

void CPBar::print() const {
    PrintFormat("[high: %.3f, low: %.3f, depth: %d] - [accHigh: %.3f, accLow: %.3f, depth: %d]", high(), low(), depth(), m_accumulation.high(), m_accumulation.low(), m_accumulation.depth());
    for (int i = 0; i < ArraySize(m_levels); i++) {
        double price = getPrice(i);
        string flag = " ";
        if (depth() > 1) {
            if (i == m_indexHigh)
                flag = "+";
            else if (i == m_indexLow)
                flag = "-";
        }

        string bidMarker = (price == SymbolInfoDouble(m_symbol.name(), SYMBOL_BID))?"[":" ";
        string askMarker = (price == SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK))?"]":" ";
        string accumulationMarker = (price == m_accumulation.price()) ? "o" : " ";
        PrintFormat("%.3f - %s%s%4.0f : %4.0f%s%s", price, flag, bidMarker, m_levels[i].sellVolume(), m_levels[i].buyVolume(), askMarker, accumulationMarker);
    }
}

bool CPBar::channeling(int pChannelingTime) const {
    return elapsedTime() > pChannelingTime;
}

/*------------------------------------------------------------------
Returns average price of the bar.

Returns:
    Average price of the bar taking into account the volume in each
    price level.
*/
double CPBar::mean() const {
    double sum = 0;
    double volume = 0;
    for(int i = m_indexHigh; i <= m_indexLow; i++) {
        sum += getPrice(i) * m_levels[i].total();
        volume += m_levels[i].total();
    }
    return sum / volume;
}

/*------------------------------------------------------------------
Returns the close price in the bar. If the bar is not closed yet,
last price is returned.

Returns:
    Close price of the bar.

*/
double CPBar::close() const {
    return m_closePrice;
}

/*------------------------------------------------------------------
Returns the opening price of the bar.

Returns:
    Opening price of the bar.
*/
double CPBar::open() const {
    return m_openPrice;
}

