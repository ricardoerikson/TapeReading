#include <TapeReading/Dom/CMarketSide.mqh>
#include <TapeReading/Utils/ArrayUtils.mqh>
#include <TapeReading/Core/CSymbol.mqh>

class CAsk: public CMarketSide
{
public:
    CAsk(CMarketBook *pBook, const CSymbol &pSymbol, double pMaxDeviation);

    bool hasLiquidityVacuum(int pDepth);
    double accumulatedVolume(int pDepth);
    double averageVolume(int pDepth = 8);
    double bestPrice();
    double deviation(long pVolume);
    double maxVolume(int pDepth);
    double maxVolumePrice(int pDepth);
    double volume(int pPos);
    int maxVolumeIndex(int pDepth = 4);
};

CAsk::CAsk(CMarketBook *pBook, const CSymbol &pSymbol, double pMaxDeviation):
    CMarketSide(pBook, pSymbol, pMaxDeviation) {
}

double CAsk::bestPrice() {
    if (exists())
        return SymbolInfoDouble(m_symbol.name(), SYMBOL_ASK);
    return -1.0;
}

double CAsk::deviation(long pVolume) {
    int bestAsk = bestIndex();
    int lastAsk = lastIndex();
    double averagePrice = 0.0;
    long volumeExec = pVolume;
    for (int i = bestAsk; i >= lastAsk; i--) {
        long currentVolume = m_marketBook.book[i].volume < volumeExec ? m_marketBook.book[i].volume : volumeExec;
        averagePrice += currentVolume * m_marketBook.book[i].price;
        volumeExec -= m_marketBook.book[i].volume;
        if (volumeExec <= 0)
            break;
    }
    if (volumeExec > 0)
        return -1.0;
    averagePrice /= (double) pVolume;
    return averagePrice - m_marketBook.book[bestAsk].price;
}

double CAsk::volume(int pPos) {
    int requiredIndex = bestIndex() - pPos;
    if (exists() && requiredIndex >= lastIndex() && requiredIndex <= bestIndex())
        return (double) m_marketBook.book[bestIndex() - pPos].volume;
    return -1.0;
}

double CAsk::averageVolume(int pDepth = 8) {
    double levels[];
    if (!exists())
        return -1;
    int begin = bestIndex();
    int end = MathMax(bestIndex() - pDepth + 1, lastIndex());
    ArrayResize(levels, begin - end + 1);
    for (int i = begin; i >= end; i--)
        levels[bestIndex() - i] = (double) m_marketBook.book[i].volume;
    ArraySort(levels);
    double volumes[];
    if (ArraySize(levels) >= 6)
        ArrayCopy(volumes, levels, 0, 2, ArraySize(levels) - 4);
    else
        ArrayCopy(volumes, levels);
    return average(volumes);
}

double CAsk::accumulatedVolume(int pDepth) {
    if (!exists())
        return -1;
    long sum = 0;
    int begin = bestIndex();
    int end = MathMax(bestIndex() - pDepth + 1, lastIndex());
    for (int i = begin; i >= end; i--)
        sum += m_marketBook.book[i].volume;
    return (double)sum;
}

bool CAsk::hasLiquidityVacuum(int pDepth) {
    if (!exists())
        return true;
    int begin = bestIndex();
    int end = MathMax(bestIndex() - pDepth + 1, lastIndex());
    double sum = 0.0;
    for (int i = end; i < begin; i++) {
        sum += MathAbs(m_marketBook.book[i].price - m_marketBook.book[i + 1].price);
    }
    if (pDepth == (int)(sum / m_symbol.tickSize()))
        return false;
    return true;
}

int CAsk::maxVolumeIndex(int pDepth) {
    if (!exists())
        return WRONG_VALUE;
    int begin = MathMax(m_bestIndex - pDepth + 1, m_lastIndex);
    int end = m_bestIndex;
    int index = WRONG_VALUE;
    long maxVol = 0.0;
    for(int i = begin; i<= end; i++) {
        if (m_marketBook.book[i].volume >= maxVol) {
            index = i;
            maxVol = m_marketBook.book[i].volume;
        }
    }
    return index;
}

double CAsk::maxVolumePrice(int pDepth) {
    if (!exists())
        return 0.0;
    int index = maxVolumeIndex(pDepth);
    if (index != WRONG_VALUE)
        return m_marketBook.book[index].price;
    return 0.0;
}

double CAsk::maxVolume(int pDepth) {
    if (!exists())
        return 0.0;
    int index = maxVolumeIndex(pDepth);
    if (index != WRONG_VALUE)
        return (double) m_marketBook.book[index].volume;
    return 0.0;
}
