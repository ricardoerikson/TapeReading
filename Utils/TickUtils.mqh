#include <TapeReading/Core/CSymbol.mqh>

int TicksCounter(CSymbol &pSymbol, double pFirstPrice, double pSecondPrice) {
    return (int) (MathAbs(pSecondPrice - pFirstPrice) / pSymbol.tickSize());
}

double TicksAdd(CSymbol &pSymbol, int pTicks, double pPrice) {
    return pPrice + pSymbol.tickSize() * pTicks;
}

bool IsBetween(MqlTick &pTick) {
    return pTick.flags == 24 && pTick.last < pTick.ask && pTick.last > pTick.bid;
}

bool IsBuyAggression(MqlTick &tick) {
    return tick.flags == 24 && tick.last >= tick.ask;
}

bool IsSellAggression(MqlTick &tick) {
    return tick.flags == 24 && tick.last <= tick.bid;
}