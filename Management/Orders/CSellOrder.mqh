#include <TapeReading/Management/Orders/COrderBase.mqh>

class CSellOrder : public COrderBase
{
public:
    CSellOrder(const CSymbol &pSymbol, IOrderListener *pListener = NULL);
    bool operation(const double pVolume, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
};

CSellOrder::CSellOrder(const CSymbol &pSymbol, IOrderListener *pListener = NULL) :
    COrderBase(pSymbol, "SELL", pListener) {

}

bool CSellOrder::operation(const double pVolume, const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    return m_trade.Sell(pVolume, m_symbol.name(), 0, pStopLoss, pTakeProfit, pComment);
}
