#include <TapeReading/Utils/MathUtils.mqh>

#include <Math/Fuzzy/mamdanifuzzysystem.mqh>

/*------------------------------------------------------------------
Fuzzy Inference System to compute market aggressiveness. It basically
takes into account the number of incoming orders when there is a
spread change and the elapsed time since the last spread change.
*/
class CAggressivenessFIS
{
public:
    CAggressivenessFIS(double tradesCount, double elapsedTime);
    ~CAggressivenessFIS();
    double evaluate();

private:
    void setInputs();
    void setOutputs();
    void setRules();
    CFuzzyVariable      *fvTradesCount, *fvElapsedTime;
    CFuzzyVariable      *fvMarket;
    CMamdaniFuzzySystem *mfs;
    CMamdaniFuzzyRule   *rule0, *rule1;
    const double MAX_TRADES_COUNT, MAX_ELAPSED_TIME;
    double mTradesCount, mElapsedTime;
};

CAggressivenessFIS::CAggressivenessFIS(double tradesCount, double elapsedTime):
    MAX_TRADES_COUNT(30.0), MAX_ELAPSED_TIME(30.0) {
    mTradesCount = BoundValue(tradesCount, 0.0, MAX_TRADES_COUNT);
    mElapsedTime = BoundValue(elapsedTime, 0.0, MAX_ELAPSED_TIME);
    mfs = new CMamdaniFuzzySystem();
    setInputs();
    setOutputs();
    setRules();
}

CAggressivenessFIS::~CAggressivenessFIS() {
    if (CheckPointer(mfs) != POINTER_INVALID) delete(mfs);
}

void CAggressivenessFIS::setInputs() {
    // Trades count
    fvTradesCount = new CFuzzyVariable("trades_count", 0.0, MAX_TRADES_COUNT);
    fvTradesCount.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(0.0, 0.0, MAX_TRADES_COUNT)));
    fvTradesCount.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(0.0, MAX_TRADES_COUNT, MAX_TRADES_COUNT)));
    mfs.Input().Add(fvTradesCount);

    // Elapsed time
    fvElapsedTime = new CFuzzyVariable("elapsed_time", 0.0, MAX_ELAPSED_TIME);
    fvElapsedTime.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(0.0, 0.0, MAX_ELAPSED_TIME)));
    fvElapsedTime.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(0.0, MAX_ELAPSED_TIME, MAX_ELAPSED_TIME)));
    mfs.Input().Add(fvElapsedTime);
}

void CAggressivenessFIS::setOutputs() {
    fvMarket = new CFuzzyVariable("market", 0.0, 1.0);
    fvMarket.Terms().Add(new CFuzzyTerm("passive", new CTriangularMembershipFunction(0.0, 0.0, 1.0)));
    fvMarket.Terms().Add(new CFuzzyTerm("aggressive", new CTriangularMembershipFunction(0.0, 1.0, 1.0)));
    mfs.Output().Add(fvMarket);
}

void CAggressivenessFIS::setRules() {
    rule0 = mfs.ParseRule("if (trades_count is high) and (elapsed_time is low) then market is aggressive");
    rule1 = mfs.ParseRule("if (trades_count is low) or (elapsed_time is high) then market is passive");
    mfs.Rules().Add(rule0);
    mfs.Rules().Add(rule1);
}

/*------------------------------------------------------------------
Evaluates to a value that defines if the market is either aggressive
or passive.

@return
    Market aggressiveness. Values closer to 0.0 indicate market
    passiveness. Values closer to 1.0 indicate a great number of
    incoming market orders.
*/
double CAggressivenessFIS::evaluate() {
    // Inputs
    CList *in = new CList;
    CDictionary_Obj_Double *p_od_tradesCount = new CDictionary_Obj_Double;
    CDictionary_Obj_Double *p_od_elapsedTime = new CDictionary_Obj_Double;

    p_od_tradesCount.SetAll(fvTradesCount, mTradesCount);
    p_od_elapsedTime.SetAll(fvElapsedTime, mElapsedTime);
    in.Add(p_od_tradesCount);
    in.Add(p_od_elapsedTime);

    // Result
    CList *result;
    CDictionary_Obj_Double *out_fis;
    result = mfs.Calculate(in);
    out_fis = result.GetNodeAtIndex(0);
    double value = ScaleValue(out_fis.Value(), 0.33, 0.66, 0.0, 1.0);

    // Deleting pointers
    if (CheckPointer(in) != POINTER_INVALID) delete(in);
    if (CheckPointer(result) != POINTER_INVALID) delete(result);
    if (CheckPointer(p_od_elapsedTime) != POINTER_INVALID) delete(p_od_elapsedTime);
    if (CheckPointer(p_od_tradesCount) != POINTER_INVALID) delete(p_od_tradesCount);
    if (CheckPointer(out_fis) != POINTER_INVALID) delete(out_fis);
    return value;
}
