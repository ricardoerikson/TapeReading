#include <TapeReading/PChart/IPBarListener.mqh>
#include <TapeReading/PChart/CPBar.mqh>
#include <TapeReading/PChart/CBarData.mqh>
#include <TapeReading/Patterns/Observer/Observable.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>
#include <TapeReading/Core/CAggressionsBag.mqh>
#include <TapeReading/Core/CAggression.mqh>

class CTimesNSales;
class CSymbol;

class CPChart : public IPBarListener
{
public:
    CPChart(CSymbol &pSymbol, CTimesNSales *pTimesNSales, int pVariations, double pAccumulationFactor = 0.7);
    ~CPChart();
    void onCloseAndOpenBar(double pPrice, int pTimesNSalesIndex);
    void addBarData(CBarData &pData);
    void update();
    double volumeImbalance() const;

    CAggressionsBag *buy();
    CAggressionsBag *sell();

    CPBar* bar();
    CBarData bars[];

private:
    void resetLargeAggressionsBags();

    CPBar *m_bar;
    CTimesNSales *m_timesNSales;
    CSymbol m_symbol;
    CAggressionsBag *m_buyBag;
    CAggressionsBag *m_sellsBag;

    double m_accumulationFactor;
    int m_variations;

};

CPChart::CPChart(CSymbol &pSymbol, CTimesNSales *pTimesNSales, int pVariations, double pAccumulationFactor) :
    m_variations(pVariations), m_symbol(pSymbol), m_accumulationFactor(pAccumulationFactor) {
    m_timesNSales = pTimesNSales;
    MqlTick tick;
    SymbolInfoTick(m_symbol.name(), tick);
    m_bar = new CPBar(m_symbol, GetPointer(this), m_timesNSales, m_variations, tick.last, m_accumulationFactor);
    m_buyBag = new CAggressionsBag();
    m_sellsBag = new CAggressionsBag();
}

CPChart::~CPChart() {
    if (CheckPointer(m_bar) != POINTER_INVALID) delete(m_bar);
    if (CheckPointer(m_buyBag) != POINTER_INVALID) delete(m_buyBag);
    if (CheckPointer(m_sellsBag) != POINTER_INVALID) delete(m_sellsBag);
    ArrayFree(bars);
}

void CPChart::onCloseAndOpenBar(double pPrice, int pTimesNSalesIndex) {
    double buyMean = buy().mean();
    double buySlope = buy().slope();
    double buyVolume = buy().volume();
    double sellMean = sell().mean();
    double sellSlope = sell().slope();
    double sellVolume = sell().volume();
    double high = m_bar.high();
    double low = m_bar.low();
    double open = m_bar.open();
    double close = m_bar.close();
    datetime openTime = m_bar.openTime();
    datetime closeTime = TimeCurrent();

    CBarData bar(low, high, open, close, buyMean, sellMean, buySlope, sellSlope, buyVolume, sellVolume, openTime, closeTime);
    Print(bar.description());
    addBarData(bar);

    if (CheckPointer(m_bar) != POINTER_INVALID) delete(m_bar);
    m_bar = new CPBar(m_symbol, GetPointer(this), m_timesNSales, m_variations, pPrice, m_accumulationFactor, pTimesNSalesIndex);
    resetLargeAggressionsBags();
}

CPBar* CPChart::bar() {
    return m_bar;
}

void CPChart::update() {
    m_bar.update();
}

void CPChart::addBarData(CBarData &pData) {
    int newSize = ArraySize(bars) + 1;
    ArraySetAsSeries(bars, false);
    ArrayResize(bars, newSize);
    bars[newSize - 1] = pData;
    ArraySetAsSeries(bars, true);
}

void CPChart::resetLargeAggressionsBags() {
    if (CheckPointer(m_buyBag) != POINTER_INVALID) delete(m_buyBag);
    if (CheckPointer(m_sellsBag) != POINTER_INVALID) delete(m_sellsBag);
    m_buyBag = new CAggressionsBag();
    m_sellsBag = new CAggressionsBag();
}

/*------------------------------------------------------------------
Large buy aggressions
*/
CAggressionsBag* CPChart::buy() {
    return m_buyBag;
}

/*------------------------------------------------------------------
Large sell aggressions
*/
CAggressionsBag* CPChart::sell() {
    return m_sellsBag;
}

double CPChart::volumeImbalance() const {
    double buyVolume = m_buyBag.volume();
    double sellVolume = m_sellsBag.volume();
    double total = buyVolume + sellVolume;
    if (total == 0)
        return 0.0;
    return (buyVolume - sellVolume) / total;
}
