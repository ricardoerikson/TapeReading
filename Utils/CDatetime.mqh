//+------------------------------------------------------------------+
//|                                 Copyright 2017, Ricardo Erikson. |
//+------------------------------------------------------------------+

#define TIME_ADD_MINUTE 60
#define TIME_ADD_HOUR 3600
#define TIME_ADD_DAY 86400

class CDatetime
{
public:
    CDatetime(const CDatetime &d);
    CDatetime(int hour, int minute = 0, int second = 0);
    CDatetime(datetime date = __DATETIME__);
    ~CDatetime();
    CDatetime addSeconds(int seconds);
    CDatetime addMinutes(int minutes);
    CDatetime addHours(int hours);
    CDatetime addDays(int days);
    datetime get() const;
    string toString();
    ulong timestamp();

    static datetime get(int hour, int minute = 0, int second = 0);

private:
    datetime mDatetime;

};

CDatetime::CDatetime(int hour, int minute, int second) {
    MqlDateTime timeStruct;
    TimeToStruct(TimeCurrent(), timeStruct);
    timeStruct.hour = hour;
    timeStruct.min = minute;
    timeStruct.sec = second;
    mDatetime = StructToTime(timeStruct);
}

CDatetime::CDatetime(const CDatetime &d) {
    mDatetime = d.mDatetime;
}

CDatetime::CDatetime(datetime date = __DATETIME__):
    mDatetime(date) {
}

CDatetime::~CDatetime() {
}

CDatetime CDatetime::addSeconds(int seconds) {
    datetime d = mDatetime + seconds;
    CDatetime newDate(d);
    return newDate;
}

CDatetime CDatetime::addMinutes(int minutes) {
    datetime d = mDatetime + minutes * TIME_ADD_MINUTE;
    CDatetime newDate(d);
    return newDate;
}

CDatetime CDatetime::addHours(int hours) {
    datetime d = mDatetime +  hours * TIME_ADD_HOUR;
    CDatetime newDate(d);
    return newDate;
}

CDatetime CDatetime::addDays(int days) {
    datetime d = mDatetime + days * TIME_ADD_DAY;
    CDatetime newDate(d);
    return newDate;
}

datetime CDatetime::get() const {
    return mDatetime;
}

string CDatetime::toString() {
    return TimeToString(mDatetime);
}

ulong CDatetime::timestamp() {
    return ((ulong) mDatetime) * 1000;
}

datetime CDatetime::get(int hour, int minute, int second) {
    CDatetime date(hour, minute, second);
    return date.get();
}