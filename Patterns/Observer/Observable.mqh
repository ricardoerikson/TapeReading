#include <TapeReading/Utils/ArrayUtils.mqh>

template<typename T>
class Observable {
public:
    void attach(T *pObserver) {
        for(int i = 0; i < ArraySize(m_observers); i++)
            if (m_observers[i] == pObserver)
                return;
        int newSize = ArraySize(m_observers) + 1;
        ArrayResize(m_observers, newSize);
        m_observers[newSize - 1] = pObserver;
    };

    void detach(T *pObserver) {
        for (int i = 0; i < ArraySize(m_observers); ++i) {
            if (m_observers[i] == pObserver) {
                ArrayRemoveIndex(m_observers, i);
                break;
            }
        }
    };

    T* observer(int i) const {
        int index = MathMin(MathMax(0, i), observersCount() - 1);
        return m_observers[index];
    }

    int observersCount() const {
        return ArraySize(m_observers);
    };

    virtual string name() const = 0;

    void detachAll() {
        ArrayFree(m_observers);
    };

    virtual void notifyObservers() = 0;

    void observe() {
        if (ArraySize(m_observers) <= 0)
            return;
        notifyObservers();
    };

protected:
    T *m_observers[];

};