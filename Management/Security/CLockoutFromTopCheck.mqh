#include <TapeReading/Management/Security/CSecurityCheck.mqh>
#include <TapeReading/Management/Trade/TradeReport.mqh>

class CLockoutFromTopCheck : public CSecurityCheck
{
public:
    CLockoutFromTopCheck(double pLockout);
    bool check();
    void report() const;

private:
    const double m_lockout;
    double m_topEquity;

};

CLockoutFromTopCheck::CLockoutFromTopCheck(double pLockout) :
    CSecurityCheck(LOCKOUT_FROM_TOP_CHECK), m_lockout(pLockout), m_topEquity(0) {
}

bool CLockoutFromTopCheck::check() {
    double equity = AccountInfoDouble(ACCOUNT_EQUITY);
    m_topEquity = MathMax(m_topEquity, equity);
    if (equity < (m_topEquity - m_lockout))
        return (false);
    return (true);
}

void CLockoutFromTopCheck::report() const {
    double equity = AccountInfoDouble(ACCOUNT_EQUITY);
    ReportSecurityCheck(m_name, StringFormat("current equity: %.2f, min equity from top: %.2f", equity, (m_topEquity - m_lockout)));
}