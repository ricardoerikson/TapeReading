#include <TapeReading/Management/Security/CSecurityCheck.mqh>

#include <TapeReading/Management/Security/CMaxEquityCheck.mqh>
#include <TapeReading/Management/Security/CLockoutCheck.mqh>
#include <TapeReading/Management/Security/CLockoutFromTopCheck.mqh>
#include <TapeReading/Management/Security/CTimeCheck.mqh>


class CGlobalSecurityCheck
{
public:
    CGlobalSecurityCheck();
    ~CGlobalSecurityCheck();
    bool pass() const;
    void enable(const string pCheck, bool pEnabled);

private:
    void addCheck(CSecurityCheck *pCheck);

    CSecurityCheck *m_checks[];

};

CGlobalSecurityCheck::CGlobalSecurityCheck() {
    CMaxEquityCheck *equityCheck = new CMaxEquityCheck(_glEquity, _glEquityPercent);
    CLockoutCheck *lockoutCheck = new CLockoutCheck(_glLockout, _glEquity);
    CLockoutFromTopCheck *lockoutFromTopCheck = new CLockoutFromTopCheck(_glLockoutFromTop);
    CTimeCheck *timeCheck = new CTimeCheck(_glOpeningHour, _glOpeningMinute, _glClosingHour, _glClosingMinute);

    addCheck(equityCheck);
    addCheck(lockoutCheck);
    addCheck(lockoutFromTopCheck);
    addCheck(timeCheck);
}

CGlobalSecurityCheck::~CGlobalSecurityCheck() {
    for (int i = 0; i < ArraySize(m_checks); i++)
        if (CheckPointer(m_checks[i]) != POINTER_INVALID) delete(m_checks[i]);
    ArrayFree(m_checks);
}

void CGlobalSecurityCheck::addCheck(CSecurityCheck *pCheck) {
    for(int i = 0; i < ArraySize(m_checks); i++)
        if (m_checks[i] == pCheck)
            return;
    int newSize = ArraySize(m_checks) + 1;
    ArrayResize(m_checks, newSize);
    m_checks[newSize - 1] = pCheck;
}

bool CGlobalSecurityCheck::pass() const {
    for (int i = 0; i < ArraySize(m_checks); i++) {
        if (!m_checks[i].pass())
            return false;
    }
    return true;
}

void CGlobalSecurityCheck::enable(const string pCheckName, bool pEnabled) {
    for(int i = 0; i < ArraySize(m_checks); ++i) {
        if (m_checks[i].name() == pCheckName) {
            m_checks[i].enable(pEnabled);
        }
    }
}