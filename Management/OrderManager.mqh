#include <Trade/Trade.mqh>
#include <TapeReading/Core/CSymbol.mqh>
#include <TapeReading/Core/Constants.mqh>
#include <TapeReading/Management/Trade/TradeReport.mqh>

#include <TapeReading/Management/Orders/IOrderListener.mqh>
#include <TapeReading/Management/Orders/CBuyOrder.mqh>
#include <TapeReading/Management/Orders/CBuyLimitOrder.mqh>
#include <TapeReading/Management/Orders/CBuyStopOrder.mqh>
#include <TapeReading/Management/Orders/CSellOrder.mqh>
#include <TapeReading/Management/Orders/CSellLimitOrder.mqh>
#include <TapeReading/Management/Orders/CSellStopOrder.mqh>


class OrderManager : public IOrderListener
{
public:
    OrderManager(const string pSymbol, uint pLots, uint pMaxLots);
    ~OrderManager();
    bool buy(const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
    bool buyLimit(const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
    bool buyStop(const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
    bool sell(const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
    bool sellLimit(const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
    bool sellStop(const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);

    bool positionModify(const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL);
    bool close();
    bool closePartial();
    bool manages(string pSymbol) const;

    void onCancel(double pVolume);
    void onSuccess(double pVolume);
    void checkLock();
    bool locked();
    bool closeMode() const;
    void unsetCloseMode();

private:
    bool checkLots();

    CBuyLimitOrder *m_buyLimitOrder;
    CBuyOrder *m_buyOrder;
    CBuyStopOrder *m_buyStopOrder;
    CSellLimitOrder *m_sellLimitOrder;
    CSellOrder *m_sellOrder;
    CSellStopOrder *m_sellStopOrder;
    CSymbol m_symbol;
    CTrade m_trade;

    bool m_closeMode;
    bool m_lockCondition;
    int m_freeLots;
    uint m_lots;
    uint m_maxLots;

};

OrderManager::OrderManager(const string pSymbol, uint pLots, uint pMaxLots) :
    m_symbol(pSymbol), m_lots(pLots), m_maxLots(pMaxLots), m_closeMode(false) {
    if (pMaxLots < pLots) {
        pLots = 0;
    }
    m_freeLots = (int) pMaxLots;
    m_buyOrder = new CBuyOrder(m_symbol, GetPointer(this));
    m_buyLimitOrder = new CBuyLimitOrder(m_symbol, GetPointer(this));
    m_buyStopOrder = new CBuyStopOrder(m_symbol, GetPointer(this));
    m_sellOrder = new CSellOrder(m_symbol, GetPointer(this));
    m_sellLimitOrder = new CSellLimitOrder(m_symbol, GetPointer(this));
    m_sellStopOrder = new CSellStopOrder(m_symbol, GetPointer(this));
}

OrderManager::~OrderManager() {
    if (CheckPointer(m_buyOrder) != POINTER_INVALID) delete(m_buyOrder);
    if (CheckPointer(m_buyLimitOrder) != POINTER_INVALID) delete(m_buyLimitOrder);
    if (CheckPointer(m_buyStopOrder) != POINTER_INVALID) delete(m_buyStopOrder);
    if (CheckPointer(m_sellOrder) != POINTER_INVALID) delete(m_sellOrder);
    if (CheckPointer(m_sellLimitOrder) != POINTER_INVALID) delete(m_sellLimitOrder);
    if (CheckPointer(m_sellStopOrder) != POINTER_INVALID) delete(m_sellStopOrder);
}

bool OrderManager::buy(const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    if (!locked() && checkLots())
        return m_buyOrder.execute(m_lots, 0, pStopLoss, pTakeProfit, pComment);
    return(false);
}

bool OrderManager::buyLimit(const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    if (!locked() && checkLots())
        return m_buyLimitOrder.execute(m_lots, pPrice, pStopLoss, pTakeProfit, pComment);
    return(false);
}

bool OrderManager::buyStop(const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    if (!locked() && checkLots())
        return m_buyStopOrder.execute(m_lots, pPrice, pStopLoss, pTakeProfit, pComment);
    return(false);
}

bool OrderManager::sell(const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    if (!locked() && checkLots())
        return m_sellOrder.execute(m_lots, 0, pStopLoss, pTakeProfit, pComment);
    return(false);
}

bool OrderManager::sellLimit(const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    if (!locked() && checkLots())
        return m_sellLimitOrder.execute(m_lots, pPrice, pStopLoss, pTakeProfit, pComment);
    return(false);
}

bool OrderManager::sellStop(const double pPrice, const double pStopLoss = 0.0, const double pTakeProfit = 0.0, const string pComment = NULL) {
    if (!locked() && checkLots())
        return m_sellStopOrder.execute(m_lots, pPrice, pStopLoss, pTakeProfit, pComment);
    return(false);
}

bool OrderManager::checkLots() {
    if (m_lots <= 0) {
        ReportAction(m_symbol, REPORT_ZERO_VOLUME_ERROR, StringFormat("%I64u available lots from %I64u", m_freeLots, m_maxLots));
        return (false);
    }
    if ((int)(m_freeLots - m_lots) < 0) {
        ReportAction(m_symbol, REPORT_MAX_VOLUME_EXCEEDED_ERROR, StringFormat("%I64u available lots from %I64u", m_freeLots, m_maxLots));
        return(false);
    }
    return (true);
}

void OrderManager::onCancel(double pVolume) {
    m_freeLots += (int) (pVolume / m_symbol.lotsStep());
    checkLock();
}

void OrderManager::onSuccess(double pVolume) {
    m_freeLots -= (int) (pVolume / m_symbol.lotsStep());
    checkLock();
}

bool OrderManager::close() {
    m_closeMode = true;
    ReportAction(m_symbol, REPORT_CLOSE_POSITION_ATTEMPT);
    bool success = m_trade.PositionClose(m_symbol.name());
    MqlTradeResult result;
    m_trade.Result(result);
    if (success) {
        m_freeLots = (int)m_maxLots;
        m_lockCondition = false;
        ReportResult(m_symbol, result, REPORT_CLOSE_POSITION_SUCCESS);
        return(true);
    }
    ReportResult(m_symbol, result, REPORT_CLOSE_POSITION_ERROR);
    m_closeMode = false;
    return(false);
}

bool OrderManager::closePartial() {
    m_closeMode = true;
    ReportAction(m_symbol, REPORT_CLOSE_PARTIAL_ATTEMPT);
    double volume = m_lots * m_symbol.lotsStep();
    bool success = m_trade.PositionClosePartial(m_symbol.name(), volume);
    MqlTradeResult result;
    m_trade.Result(result);
    if (success) {
        m_freeLots += (int)(volume / m_symbol.lotsStep());
        m_lockCondition = false;
        ReportResult(m_symbol, result, REPORT_CLOSE_PARTIAL_SUCCESS);
        return(true);
    }
    ReportResult(m_symbol, result, REPORT_CLOSE_PARTIAL_ERROR);
    m_closeMode = false;
    return(false);
}

void OrderManager::checkLock() {
    if (m_freeLots <= 0) {
        ReportAction(m_symbol, REPORT_MAX_VOLUME_EXCEEDED_ERROR, StringFormat("%I64u available lots from %I64u", m_freeLots, m_maxLots), TRADE_REPORT_ALERT);
        // TODO: Add email and alert
        m_lockCondition = true;
    }
}

bool OrderManager::locked() {
    return m_lockCondition;
}

bool OrderManager::positionModify(const double pStopLoss, const double pTakeProfit, const string pComment) {
    ReportAction(m_symbol, REPORT_MODIFY_POSITION_ATTEMPT, StringFormat("sl: %.3f, tp: %.3f", pStopLoss, pTakeProfit));
    bool success = m_trade.PositionModify(m_symbol.name(), pStopLoss, pTakeProfit);
    MqlTradeResult result;
    m_trade.Result(result);
    if (success) {
        ReportResult(m_symbol, result, REPORT_MODIFY_POSITION_SUCCESS);
        return(true);
    }
    ReportResult(m_symbol, result, REPORT_MODIFY_POSITION_ERROR);
    return(false);
}

bool OrderManager::manages(string pSymbol) const {
    return m_symbol.name() == pSymbol;
}

void OrderManager::unsetCloseMode() {
    m_closeMode = false;
}

bool OrderManager::closeMode() const {
    return m_closeMode;
}