#include <TapeReading/TimesNSales/CImbalance.mqh>
#include <TapeReading/Utils/MathUtils.mqh>

#include <Math/Fuzzy/mamdanifuzzysystem.mqh>

/*------------------------------------------------------------------
Fuzzy Inference System to compute flow imbalance by taking into
account the following parameters: overall volume imbalance, overall
trades imbalance, filtered volume imbalance and filtered trades
imbalance.
*/
class CFlowImbalanceFIS
{
public:
    CFlowImbalanceFIS(CImbalance &overall, CImbalance &filtered);
    ~CFlowImbalanceFIS();
    double evaluate();

private:
    void setInputs();
    void setOutputs();
    void setRules();

    CImbalance mOverall, mFiltered;
    double mTradesCount, mElapsedTime;
    CFuzzyVariable *fvOverallVolumeImbalance, *fvFilteredVolumeImbalance;
    CFuzzyVariable *fvAction;
    CMamdaniFuzzySystem *mfs;
    CMamdaniFuzzyRule *rule0, *rule1, *rule2, *rule3, *rule4;

};

CFlowImbalanceFIS::CFlowImbalanceFIS(CImbalance &overall, CImbalance &filtered):
    mOverall(overall), mFiltered(filtered) {
    mfs = new CMamdaniFuzzySystem();
    setInputs();
    setOutputs();
    setRules();
}

CFlowImbalanceFIS::~CFlowImbalanceFIS() {
    if (CheckPointer(mfs) != POINTER_INVALID) delete(mfs);
}

void CFlowImbalanceFIS::setInputs() {
    // Overall volume imbalance
    fvOverallVolumeImbalance = new CFuzzyVariable("overall_vi", -1.0, 1.0);
    fvOverallVolumeImbalance.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(-1.0, -1.0, 0.0)));
    fvOverallVolumeImbalance.Terms().Add(new CFuzzyTerm("average", new CTriangularMembershipFunction(-1.0, 0.0, 1.0)));
    fvOverallVolumeImbalance.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(0.0, 1.0, 1.0)));
    mfs.Input().Add(fvOverallVolumeImbalance);

    // Filtered volume imbalance
    fvFilteredVolumeImbalance = new CFuzzyVariable("filtered_vi", -1.0, 1.0);
    fvFilteredVolumeImbalance.Terms().Add(new CFuzzyTerm("low", new CTriangularMembershipFunction(-1.0, -1.0, 0.0)));
    fvFilteredVolumeImbalance.Terms().Add(new CFuzzyTerm("average", new CTriangularMembershipFunction(-1.0, 0.0, 1.0)));
    fvFilteredVolumeImbalance.Terms().Add(new CFuzzyTerm("high", new CTriangularMembershipFunction(0.0, 1.0, 1.0)));
    mfs.Input().Add(fvFilteredVolumeImbalance);

}

void CFlowImbalanceFIS::setOutputs() {
    fvAction = new CFuzzyVariable("action", -1.0, 1.0);
    fvAction.Terms().Add(new CFuzzyTerm("short", new CTriangularMembershipFunction(-1.0, -1.0, 0.0)));
    fvAction.Terms().Add(new CFuzzyTerm("do_nothing", new CTriangularMembershipFunction(-1.0, 0.0, 1.0)));
    fvAction.Terms().Add(new CFuzzyTerm("long", new CTriangularMembershipFunction(0.0 , 1.0, 1.0)));
    mfs.Output().Add(fvAction);
}

void CFlowImbalanceFIS::setRules() {
    rule0 = mfs.ParseRule("if (overall_vi is low) and (filtered_vi is low) then (action is short)");
    rule1 = mfs.ParseRule("if (overall_vi is high) and (filtered_vi is high) then (action is long)");
    rule2 = mfs.ParseRule("if (overall_vi is average) then (action is do_nothing)");
    rule3 = mfs.ParseRule("if (overall_vi is high) and (filtered_vi is not high) then (action is do_nothing)");
    rule4 = mfs.ParseRule("if (overall_vi is low) and (filtered_vi is not low) then (action is do_nothing)");
    mfs.Rules().Add(rule0);
    mfs.Rules().Add(rule1);
    mfs.Rules().Add(rule2);
    mfs.Rules().Add(rule3);
    mfs.Rules().Add(rule4);
}

/*------------------------------------------------------------------
Evaluates the fuzzy rules to a value of market imbalance.

@return
    Market imbalance that was inferenced by the system. Values closer
    to -1 indicate more market sells. Values closer to 1 indicate
    more market buys. Values closer to 0 indicate the market is balanced
    or undefined.
*/
double CFlowImbalanceFIS::evaluate() {
    // Inputs
    CList *in = new CList;
    CDictionary_Obj_Double *in_overallVolumeImbalance = new CDictionary_Obj_Double;
    CDictionary_Obj_Double *in_filteredVolumeImbalance = new CDictionary_Obj_Double;
    in_overallVolumeImbalance.SetAll(fvOverallVolumeImbalance, mOverall.volume());
    in_filteredVolumeImbalance.SetAll(fvFilteredVolumeImbalance, mFiltered.volume());
    in.Add(in_overallVolumeImbalance);
    in.Add(in_filteredVolumeImbalance);

    // Result
    CList *result;
    CDictionary_Obj_Double *out_fis;
    result = mfs.Calculate(in);
    out_fis = result.GetNodeAtIndex(0);
    double value = ScaleValue(out_fis.Value(), -0.66, 0.66, -1.0, 1.0);

    // Deleting pointers
    if (CheckPointer(in) != POINTER_INVALID) delete(in);
    if (CheckPointer(result) != POINTER_INVALID) delete(result);
    if (CheckPointer(in_overallVolumeImbalance) != POINTER_INVALID) delete(in_overallVolumeImbalance);
    if (CheckPointer(in_filteredVolumeImbalance) != POINTER_INVALID) delete(in_filteredVolumeImbalance);
    if (CheckPointer(out_fis) != POINTER_INVALID) delete(out_fis);
    return value;
}
