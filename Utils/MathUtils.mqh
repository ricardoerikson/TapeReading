double ScaleValue(double rawValue, double minRaw, double maxRaw, double minScale, double maxScale) {
    double correctedRaw = MathMax(MathMin(rawValue, maxRaw), minRaw);
    double percent = (correctedRaw - minRaw) / (maxRaw - minRaw);
    return NormalizeDouble(minScale + (maxScale - minScale) *percent, 2);
}

template <typename T>
T BoundValue(T rawValue, T lowerBound, T upperBound) {
    return MathMin(MathMax(lowerBound, rawValue), upperBound);
}

double Imbalance(double pA, double pB) {
    double denominator = (pA + pB);
    if (denominator == 0)
        return 0.0;
    return (pA - pB) / denominator;
}