#include <TapeReading/Signals/IScalpingObserver.mqh>

#include <TapeReading/Patterns/Observer/Observable.mqh>

#include <TapeReading/Dom/CDepthOfMarket.mqh>

#include <TapeReading/Core/CSymbol.mqh>

#include <TapeReading/TimesNSales/CImbalance.mqh>
#include <TapeReading/TimesNSales/CTNSTradesInfo.mqh>
#include <TapeReading/TimesNSales/CTimesNSales.mqh>

#include <TapeReading/Signals/OrderFlow/CFlowDirectionFIS.mqh>

class COrderFlowObservable: public Observable<IScalpingObserver>
{
public:
    COrderFlowObservable(CSymbol &pSymbol, CDepthOfMarket *pDom, int pImbalancePercent, int pImbalanceMinDiff, int pFilterVolume = 0);
    ~COrderFlowObservable();

    double flowDirection();
    string name() const;
    void notifyObservers();
    void resetTimer();

private:
    void notifyFlowStrength(double pStrength);
    CTimesNSales *m_timesNSales;
    CTNSTradesInfo *m_tradesInfo;
    CDepthOfMarket *m_dom;

    datetime m_since;
    double m_previousBid, m_previousAsk;

};

COrderFlowObservable::COrderFlowObservable(CSymbol &pSymbol, CDepthOfMarket *pDom, int pImbalancePercent, int pImbalanceMinDiff, int pFilterVolume) {
    m_dom = pDom;
    m_timesNSales = new CTimesNSales(pSymbol);
    m_tradesInfo = new CTNSTradesInfo(m_timesNSales, pImbalancePercent, pImbalanceMinDiff);
    m_since = TimeCurrent();
}

COrderFlowObservable::~COrderFlowObservable() {
    if (CheckPointer(m_tradesInfo) != POINTER_INVALID) delete(m_tradesInfo);
    if (CheckPointer(m_timesNSales) != POINTER_INVALID) delete(m_timesNSales);
}

string COrderFlowObservable::name() const {
    return "Order Flow Observable";
}

void COrderFlowObservable::notifyObservers() {
    m_timesNSales.retrieveSince(m_since);
}

double COrderFlowObservable::flowDirection() {
    double spreadImbalance = m_dom.imbalance(1);
    double volumeOrderImbalance = m_tradesInfo.imbalance().volume();
    CFlowDirectionFIS cfs(spreadImbalance, volumeOrderImbalance);
    return cfs.evaluate();
}

void COrderFlowObservable::resetTimer() {
    m_since = TimeCurrent();
}
