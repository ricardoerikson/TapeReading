#include <TapeReading/TimesNSales/CTimesNSales.mqh>
#include <TapeReading/TimesNSales/CImbalance.mqh>

class CTNSTradesInfo
{
public:
    CTNSTradesInfo(CTimesNSales *tns);
    CTNSTradesInfo(CTimesNSales *tns, int percent, int minDiff);
    ~CTNSTradesInfo();
    CImbalance imbalance(ulong filter = 0);
    double currentPrice();
    double lastPrice();
    uint interval();
    uint ticksCount();
    double averagePrice();

private:
    CTimesNSales *mTimesNSales;
    int mPercent, mMinDiff;
};

CTNSTradesInfo::CTNSTradesInfo(CTimesNSales *tns, int percent, int minDiff):
    mPercent(percent), mMinDiff(minDiff) {
    mTimesNSales = tns;
}

CTNSTradesInfo::CTNSTradesInfo(CTimesNSales *tns) {
    mTimesNSales = tns;
}

CTNSTradesInfo::~CTNSTradesInfo() {
}

CImbalance CTNSTradesInfo::imbalance(ulong filter = 0) {
    CImbalance imbalance(mPercent, mMinDiff);
    for (int i = 0; i < ArraySize(mTimesNSales.trades); ++i) {
        MqlTick tick = mTimesNSales.trades[i];
        if (tick.volume < filter)
            continue;
        bool isBuy = tick.flags == 24 && tick.last >= tick.ask;
        bool isSell = tick.flags == 24 && tick.last <= tick.bid;
        if (isBuy) {
            imbalance.addBuy(tick.volume);
        } else if (isSell) {
             imbalance.addSell(tick.volume);
        }
    }
    return imbalance;
}

uint CTNSTradesInfo::interval() {
    int arraySize = ArraySize(mTimesNSales.trades);
    datetime lastUpdate = mTimesNSales.trades[0].time;
    datetime firstUpdate = mTimesNSales.trades[arraySize - 1].time;
    return (uint)(lastUpdate - firstUpdate);
}

uint CTNSTradesInfo::ticksCount() {
    return ArraySize(mTimesNSales.trades);
}

double CTNSTradesInfo::averagePrice() {
    double sum = 0.0;
    ulong volume = 0.0;
    for (int i = 0; i < ArraySize(mTimesNSales.trades); i++) {
        MqlTick tick = mTimesNSales.trades[i];
        sum += tick.volume * tick.last;
        volume += tick.volume;
    }
    if (volume == 0)
      return 0.0;
    return sum / volume;
}

double CTNSTradesInfo::currentPrice() {
    if (ArraySize(mTimesNSales.trades) == 0)
        return 0.0;
    return mTimesNSales.trades[0].last;
}

double CTNSTradesInfo::lastPrice() {
    int size = ArraySize(mTimesNSales.trades);
    if (size == 0)
        return 0.0;
    return mTimesNSales.trades[size - 1].last;
}