#include <TapeReading/Core/CSymbol.mqh>
#include <TapeReading/Utils/TickUtils.mqh>
#include <TapeReading/Patterns/Observer/Observable.mqh>
#include <TapeReading/Dom/CDepthOfMarket.mqh>
#include <TapeReading/TimesNSales/CTimesNSales.mqh>
#include <TapeReading/TimesNSales/CTNSTradesInfo.mqh>
#include <TapeReading/Signals/SupportResistance/CSupportResistanceFIS.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>

class CSupportResistanceObservable : public Observable<IScalpingObserver>
{
public:
    CSupportResistanceObservable(CSymbol &pSymbol, CDepthOfMarket *pDom, int pDepth = 4, double pBarrierFactor = 2.0, double pMinVolume = 30);
    ~CSupportResistanceObservable();
    void notifyObservers();
    string name() const;

private:
    void notifyNextToSupport(double pSupport);
    void notifyNextToResistance(double pResistance);
    void notifyOnTheVergeOfBreakingSupportLevel(double pSupport, double pImbalance);
    void notifyOnTheVergeOfBreakingResistanceLevel(double pResistance, double pImbalance);

    CSymbol         m_symbol;
    CDepthOfMarket *m_dom;
    CTimesNSales   *m_timesNSales;
    CTNSTradesInfo *m_tradesInfo;

    bool         m_breakMode;
    const double m_barrierFactor;
    const double m_minVolume;
    datetime     m_since;
    double       m_resistance;
    double       m_support;
    int          m_depth;

};

CSupportResistanceObservable::CSupportResistanceObservable(CSymbol &pSymbol, CDepthOfMarket *pDom, int pDepth, double pBarrierFactor, double pMinVolume) :
        m_symbol(pSymbol),
        m_depth(pDepth),
        m_minVolume(pMinVolume),
        m_barrierFactor(pBarrierFactor) {
    m_dom = pDom;
    m_timesNSales = new CTimesNSales(m_symbol);
    m_tradesInfo = new CTNSTradesInfo(m_timesNSales, 100, 50);
}

CSupportResistanceObservable::~CSupportResistanceObservable() {
    if (CheckPointer(m_tradesInfo) != POINTER_INVALID) delete(m_tradesInfo);
    if (CheckPointer(m_timesNSales) != POINTER_INVALID) delete(m_timesNSales);
}
CTimesNSales *m_timesNSales;


void CSupportResistanceObservable::notifyObservers() {
    double bid = m_dom.bid().bestPrice();
    double ask = m_dom.ask().bestPrice();
    if (ask > m_resistance || TicksCounter(m_symbol, ask, m_resistance) > 1) {
        m_resistance = 0.0;
    }
    if (bid < m_support || TicksCounter(m_symbol, bid, m_support) > 1) {
        m_support = 0.0;
    }
    double highestBid = m_dom.bid().maxVolume(m_depth);
    double highestAsk = m_dom.ask().maxVolume(m_depth);
    double highestBidPrice = m_dom.bid().maxVolumePrice(m_depth);
    double highestAskPrice = m_dom.ask().maxVolumePrice(m_depth);
    double meanBid = m_dom.bid().averageVolume();
    double meanAsk = m_dom.ask().averageVolume();
    double volumeAtBid = m_dom.bid().volume(0);
    double volumeAtAsk = m_dom.ask().volume(0);

    // Set support price only if the following conditions are met.
    if (bid == highestBidPrice && highestBid/meanBid >= m_barrierFactor && m_support == 0.0) {
        if (m_breakMode == false)
            m_since = TimeCurrent();
        m_breakMode = true;
        m_support = bid;
        notifyNextToSupport(m_support);
    }
    if (ask == highestAskPrice && highestAsk/meanAsk >= m_barrierFactor && m_resistance == 0.0) {
        if (m_breakMode == false)
            m_since = TimeCurrent();
        m_breakMode = true;
        m_resistance = ask;
        notifyNextToResistance(m_resistance);
    }
    if (m_breakMode == true && m_support == 0 && m_resistance == 0) {
        m_breakMode = false;
        return;
    }
    if (m_breakMode && m_timesNSales.retrieveSince(m_since)) {
        double imbalance = m_tradesInfo.imbalance().volume();
        if (volumeAtAsk < m_minVolume && imbalance > 0 && m_resistance == ask)
            notifyOnTheVergeOfBreakingResistanceLevel(m_resistance, imbalance);
        if (volumeAtBid < m_minVolume && imbalance < 0 && m_support == bid)
            notifyOnTheVergeOfBreakingSupportLevel(m_support, imbalance);
    }
}

void CSupportResistanceObservable::notifyNextToSupport(double pPrice) {
    for (int i = 0; i < ArraySize(m_observers); ++i)
        observer(i).onNextToSupportLevel(pPrice);
}

void CSupportResistanceObservable::notifyNextToResistance(double pPrice) {
    for (int i = 0; i < ArraySize(m_observers); ++i)
        observer(i).onNextToResistanceLevel(pPrice);
}

void CSupportResistanceObservable::notifyOnTheVergeOfBreakingSupportLevel(double pSupport, double pImbalance) {
    for (int i = 0; i < observersCount(); ++i)
        observer(i).onTheVergeOfBreakingSupportLevel(pSupport, pImbalance);
}

void CSupportResistanceObservable::notifyOnTheVergeOfBreakingResistanceLevel(double pResistance, double pImbalance) {
    for (int i = 0; i < observersCount(); ++i)
        observer(i).onTheVergeOfBreakingResistanceLevel(pResistance, pImbalance);
}

string CSupportResistanceObservable::name() const {
    return "Support & Resistance Observable";
}