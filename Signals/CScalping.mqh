#include <TapeReading/Core/CAggression.mqh>
#include <TapeReading/Core/Constants.mqh>
#include <TapeReading/Core/CSymbol.mqh>
#include <TapeReading/Core/Enums.mqh>
#include <TapeReading/Management/CTradesDirector.mqh>
#include <TapeReading/Management/Trade/TradeReport.mqh>
#include <TapeReading/PChart/CPChart.mqh>
#include <TapeReading/Signals/AggressionLeftover/CAggressionLeftoverObservable.mqh>
#include <TapeReading/Signals/CScalping.mqh>
#include <TapeReading/Signals/CScalpingWatcher.mqh>
#include <TapeReading/Signals/DepthImbalance/CDepthImbalanceObservable.mqh>
#include <TapeReading/Signals/EndOfQueue/CEndOfQueueObservable.mqh>
#include <TapeReading/Signals/FrontRunning/CFrontRunningObservable.mqh>
#include <TapeReading/Signals/HiddenLiquidity/CHiddenLiquidityObservable.mqh>
#include <TapeReading/Signals/LargeAggression/CLargeAggressionObservable.mqh>
#include <TapeReading/Signals/OrderFlow/COrderFlowObservable.mqh>
#include <TapeReading/Signals/PChart/CPChartObservable.mqh>
#include <TapeReading/Signals/Spread/CSpreadObservable.mqh>
#include <TapeReading/Signals/SupportResistance/CSupportResistanceObservable.mqh>
#include <TapeReading/Strategies/Channeling/CChannelingBuyStrategy.mqh>
#include <TapeReading/Strategies/CStrategy.mqh>
#include <TapeReading/Strategies/CStrategy.mqh>

class CDepthOfMarket;
class CTimesNSales;

class CScalping : public IScalpingObserver
{
public:
    CScalping(CTradesDirector *pTradesDirector, CSymbol &pSymbol, CDepthOfMarket *pDom, CTimesNSales *pTimesNSales, CPChart *pPChart, double pLeftoverFactor, double pEoQMin, double pEoQMax = 100,
        int pFlowImbalancePercent = 150, int pFlowMinDiff  = 50, int pFlowFilter = 0);
    ~CScalping();
    CScalpingWatcher* watcher();

    string state(string pStrategy = NULL) const;

    void onAccumulationZoneChanged(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh);
    void onDepthImbalance(double pImbalance);
    void onEndOfQueueAtBid(double pPrice, double pVolume, double pFactor);
    void onEndOfQueueAtAsk(double pPrice, double pVolume, double pFactor);
    void onEnteringAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh, int pDirection);
    void onHiddenLiquidityAtAsk(double pPrice, double pVolume);
    void onHiddenLiquidityAtBid(double pPrice, double pVolume);
    void onLeftoverOnAsk(double pLeftoverVolume, double pExecutionPrice);
    void onLeftoverOnBid(double pLeftoverVolume, double pExecutionPrice);
    void onOutOfAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh, int pDirection);
    void onSpreadAcceptance(datetime pSince, int pElapsedTime, double pLastBid, double pLastAsk);
    void onSpreadChanged(datetime pWhen, double pBid, double pAsk, int pDirection);
    void onSpreadOpened(double pBestBid, double pBestAsk);
    void onNextToSupportLevel(double pSupport);
    void onNextToResistanceLevel(double pResistance);
    void onTheVergeOfBreakingSupportLevel(double pSupport, double pImbalance);
    void onTheVergeOfBreakingResistanceLevel(double pResistance, double pImbalance);
    void onLargeAggression(CAggression &pAggression);
    void onFrontRunningBuy();
    void onFrontRunningSell();

private:
    bool useStrategy(CStrategy *pStrategy);
    void deleteStrategy();

    CScalpingWatcher *m_watcher;
    CTradesDirector *m_tradesDirector;
    CDepthOfMarket *m_dom;
    CSymbol m_symbol;
    CStrategy *m_strategy;
    CPChart *m_pChart;

    CAggressionLeftoverObservable *m_aggressionLeftoverObservable;
    CDepthImbalanceObservable     *m_depthImbalanceObservable;
    CEndOfQueueObservable         *m_endOfQueueObservable;
    CHiddenLiquidityObservable    *m_hiddenLiquidityObservable;
    CPChartObservable             *m_pChartObservable;
    CSpreadObservable             *m_spreadObservable;
    COrderFlowObservable          *m_orderFlowObservable;
    CSupportResistanceObservable  *m_supportResistanceObservable;
    CLargeAggressionObservable    *m_largeAggressionObservable;
    CFrontRunningObservable       *m_frontRunningObservable;

};

CScalping::CScalping(CTradesDirector *pTradesDirector, CSymbol &pSymbol, CDepthOfMarket *pDom, CTimesNSales *pTimesNSales, CPChart *pPChart, double pLeftoverFactor, double pEoQMin, double pEoQMax,
    int pFlowImbalancePercent, int pFlowMinDiff, int pFlowFilter) :
        m_symbol(pSymbol) {
    m_tradesDirector = pTradesDirector;
    m_dom = pDom;
    m_pChart = pPChart;
    Print("\n");
    Print("---------------------- Initializing scalping observer ----------------------\n");

    m_watcher = new CScalpingWatcher();

    Print("@ Initializing observables...");
    // m_aggressionLeftoverObservable = new CAggressionLeftoverObservable(pTimesNSales, pDom, pLeftoverFactor);
    m_spreadObservable             = new CSpreadObservable(pDom);
    // m_depthImbalanceObservable     = new CDepthImbalanceObservable(pDom);
    // m_endOfQueueObservable         = new CEndOfQueueObservable(pSymbol, pDom, pEoQMin, pEoQMax);
    // m_hiddenLiquidityObservable    = new CHiddenLiquidityObservable(pTimesNSales, pDom);
    // m_orderFlowObservable          = new COrderFlowObservable(pSymbol, pDom, pFlowImbalancePercent, pFlowMinDiff, pFlowFilter);
    m_pChartObservable              = new CPChartObservable(pSymbol, pTimesNSales, pPChart);
    m_frontRunningObservable       = new CFrontRunningObservable(pSymbol, pPChart);
    // m_supportResistanceObservable  = new CSupportResistanceObservable(pSymbol, pDom, 4, 1.8, 50);
    m_largeAggressionObservable    = new CLargeAggressionObservable(pTimesNSales, 80);

    Print("@ Attaching observables...");
    // m_aggressionLeftoverObservable.attach(GetPointer(this));
    m_spreadObservable.attach(GetPointer(this));
    // m_endOfQueueObservable.attach(GetPointer(this));
    // m_hiddenLiquidityObservable.attach(GetPointer(this));
    m_pChartObservable.attach(GetPointer(this));
    // m_orderFlowObservable.attach(GetPointer(this));
    // m_depthImbalanceObservable.attach(GetPointer(this)); // add to onBookEvent
    // m_supportResistanceObservable.attach(GetPointer(this));
    m_frontRunningObservable.attach(GetPointer(this));
    m_largeAggressionObservable.attach(GetPointer(this));

    Print("@ Attaching observables to events...");
    // m_watcher.addToOnTickEvent(m_aggressionLeftoverObservable);
    // m_watcher.addToOnTickEvent(m_endOfQueueObservable);
    // m_watcher.addToOnTickEvent(m_hiddenLiquidityObservable);
    m_watcher.addToOnTickEvent(m_pChartObservable);
    // m_watcher.addToOnTickEvent(m_orderFlowObservable);
    // m_watcher.addToOnTickEvent(m_supportResistanceObservable);
    m_watcher.addToOnTickEvent(m_largeAggressionObservable);
    m_watcher.addToOnTickEvent(m_frontRunningObservable);

    m_watcher.addToOnBookEvent(m_spreadObservable);
    // m_watcher.addToOnBookEvent(m_depthImbalanceObservable);
    Print("@ CScalping observer initialized.");
}

CScalping::~CScalping() {
    if (CheckPointer(m_watcher) != POINTER_INVALID) delete(m_watcher);

    if (CheckPointer(m_aggressionLeftoverObservable) != POINTER_INVALID) delete(m_aggressionLeftoverObservable);
    if (CheckPointer(m_depthImbalanceObservable) != POINTER_INVALID) delete(m_depthImbalanceObservable);
    if (CheckPointer(m_endOfQueueObservable) != POINTER_INVALID) delete(m_endOfQueueObservable);
    if (CheckPointer(m_hiddenLiquidityObservable) != POINTER_INVALID) delete(m_hiddenLiquidityObservable);
    if (CheckPointer(m_pChartObservable) != POINTER_INVALID) delete(m_pChartObservable);
    if (CheckPointer(m_spreadObservable) != POINTER_INVALID) delete(m_spreadObservable);
    if (CheckPointer(m_orderFlowObservable) != POINTER_INVALID) delete(m_orderFlowObservable);
    if (CheckPointer(m_supportResistanceObservable) != POINTER_INVALID) delete(m_supportResistanceObservable);
    if (CheckPointer(m_largeAggressionObservable) != POINTER_INVALID) delete(m_largeAggressionObservable);
    if (CheckPointer(m_frontRunningObservable) != POINTER_INVALID) delete(m_frontRunningObservable);
}

CScalpingWatcher* CScalping::watcher() {
    return m_watcher;
}

void CScalping::onLeftoverOnBid(double pLeftoverVolume, double pExecutionPrice) {
    // Print("---------------------------------------------------------------");
    // PrintFormat("Leftover on BID - price: %.3f - leftover: %.0f", pExecutionPrice, pLeftoverVolume);
    // m_orderFlowObservable.resetTimer();
    // PrintFormat("Registering end of line for ASK at: %.3f", m_dom.ask().bestPrice());
    // m_endOfQueueObservable.setAskListener(m_dom.ask().bestPrice());
    // Print("---------------------------------------------------------------");
}

void CScalping::onLeftoverOnAsk(double pLeftoverVolume, double pExecutionPrice) {
    // Print("---------------------------------------------------------------");
    // PrintFormat("Leftover on ASK - price: %.3f - leftover: %.0f", pExecutionPrice, pLeftoverVolume);
    // m_orderFlowObservable.resetTimer();
    // PrintFormat("Registering end of line for BID at: %.3f", m_dom.bid().bestPrice());
    // m_endOfQueueObservable.setAskListener(m_dom.bid().bestPrice());
    // Print("---------------------------------------------------------------");
}

void CScalping::onEndOfQueueAtBid(double pPrice, double pVolume, double pFactor) {
    // PrintFormat("End of queue on BID at %.3f, volume: %.0f", pPrice, pVolume);
}

void CScalping::onEndOfQueueAtAsk(double pPrice, double pVolume, double pFactor) {
    // PrintFormat("End of queue on ASK at %.3f, volume: %.0f", pPrice, pVolume);

}

void CScalping::onHiddenLiquidityAtBid(double pPrice, double pVolume) {
    // Print("---------------------------------------------------------------");
    // PrintFormat("Iceberg at BID: %.3f - Volume: %.0f.", pPrice, pVolume);
    // m_orderFlowObservable.resetTimer();
    // PrintFormat("Registering end of line for ask at: %.3f", m_dom.ask().bestPrice());
    // m_endOfQueueObservable.setAskListener(m_dom.ask().bestPrice());
    // Print("---------------------------------------------------------------");
}

void CScalping::onHiddenLiquidityAtAsk(double pPrice, double pVolume) {
    // Print("---------------------------------------------------------------");
    // PrintFormat("Iceberg at BID: %.3f - Volume: %.0f.", pPrice, pVolume);
    // m_orderFlowObservable.resetTimer();
    // PrintFormat("Registering end of line for bid at: %.3f", m_dom.bid().bestPrice());
    // m_endOfQueueObservable.setBidListener(m_dom.bid().bestPrice());
    // Print("---------------------------------------------------------------");
}

void CScalping::onSpreadChanged(datetime pWhen, double pBid, double pAsk, int pDirection) {
    // m_pChartObservable.chart().bar().print();
}

void CScalping::onSpreadAcceptance(datetime pSince, int pElapsedTime, double pLastBid, double pLastAsk) {
    // m_pChartObservable.chart().bar().print();
}

void CScalping::onSpreadOpened(double bestBid, double bestAsk) {

}

void CScalping::onEnteringAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh, int pDirection) {
}

void CScalping::onOutOfAccumulationZone(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh, int pDirection) {
    // m_orderFlowObservable.observeMode(true);
}

void CScalping::onAccumulationZoneChanged(double pAccumLow, double pAccumHigh, double pAccumPrice, double pLow, double pHigh) {
    // int barDepth = m_pChartObservable.chart().bar().depth();
    // int accumulationDepth = m_pChartObservable.chart().bar().accumulation().depth();
    // PrintFormat("[low: %.3f, high: %.3f, depth: %d] - [accLow: %.3f, accHigh: %.3f, depth: %d]", pLow, pHigh, barDepth, pAccumLow, pAccumHigh, accumulationDepth);
}

void CScalping::onDepthImbalance(double pImbalance) {
    // PrintFormat("imbalance: %.2f", pImbalance);
}

void CScalping::onNextToSupportLevel(double pSupport) {
    // PrintFormat("Next to support level: %.3f", pSupport);
}

void CScalping::onNextToResistanceLevel(double pResistance) {
    // PrintFormat("Next to resistance level: %.3f", pResistance);
}

void CScalping::onTheVergeOfBreakingSupportLevel(double pSupport, double pImbalance) {
    // PrintFormat("Breaking support at: %.3f , imbalance: %.2f", pSupport, pImbalance);
}

void CScalping::onTheVergeOfBreakingResistanceLevel(double pResistance, double pImbalance) {
    // PrintFormat("Breaking resistance at: %.3f , imbalance: %.2f", pResistance, pImbalance);
}

void CScalping::onLargeAggression(CAggression &pAggression) {
    if (pAggression.type() == BUY_AGGRESSION) {
        if (m_pChart.buy().low() != WRONG_VALUE && pAggression.price() < m_pChart.buy().low()) {
            m_pChart.buy().reset();
            Print("Reseting large BUY  aggressions. Size: ", m_pChart.buy().count());
        }
        m_pChart.buy().addAggression(pAggression);
    }
    else if (pAggression.type() == SELL_AGGRESSION) {
        if (m_pChart.buy().high() != WRONG_VALUE && pAggression.price() > m_pChart.sell().high()) {
            m_pChart.sell().reset();
            Print("Reseting large SELL aggressions. Size: ", m_pChart.sell().count());
        }
        m_pChart.sell().addAggression(pAggression);
    }
    PrintFormat("%s: %.3f, volume: %.0f", pAggression.stype(), pAggression.price(), pAggression.volume());
}

void CScalping::onFrontRunningBuy() {
    string comment = state();
    ReportAction(m_symbol, STRATEGY_FRONT_RUNNING_BUY, comment);
    m_tradesDirector.buy(0.0, 0.0, state(STRATEGY_FRONT_RUNNING_BUY));
}

void CScalping::onFrontRunningSell() {
    string comment = state();
    ReportAction(m_symbol, STRATEGY_FRONT_RUNNING_SELL, comment);
    m_tradesDirector.sell(0.0, 0.0, state(STRATEGY_FRONT_RUNNING_SELL));
}

string CScalping::state(string pStrategy) const {
    string tpl = "bid: %.3f, ask: %.3f, dom_imbalance: %.2f";
    double bid = m_dom.bid().bestPrice();
    double ask = m_dom.ask().bestPrice();
    double domImbalance = m_dom.imbalance(5, true);
    string status = StringFormat(tpl, bid, ask, domImbalance);
    if (pStrategy != NULL)
        return StringFormat("%s (%s)", pStrategy, status);
    return status;
}

bool CScalping::useStrategy(CStrategy *pStrategy) {
    if (CheckPointer(m_strategy) != POINTER_INVALID && m_strategy.hold())
        return false;
    if (CheckPointer(m_strategy) != POINTER_INVALID) delete(m_strategy);
    m_strategy = pStrategy;
    return true;
}

void CScalping::deleteStrategy() {
    if (CheckPointer(m_strategy) != POINTER_INVALID) delete(m_strategy);
}