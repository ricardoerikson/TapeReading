#include <TapeReading/Patterns/Observer/Observable.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>

class CScalpingWatcher
{
public:
    CScalpingWatcher(){};
    ~CScalpingWatcher();
    void addToOnTickEvent(Observable<IScalpingObserver> *pObservable);
    void addToOnBookEvent(Observable<IScalpingObserver> *pObservable);
    void observeOnBookEvent();
    void observeOnTickEvent();

private:
    Observable<IScalpingObserver> *m_onTickEventObservables[];
    Observable<IScalpingObserver> *m_onBookEventObservables[];

};

void CScalpingWatcher::addToOnTickEvent(Observable<IScalpingObserver> *pObservable) {
    for(int i = 0; i < ArraySize(m_onTickEventObservables); i++)
        if (m_onTickEventObservables[i] == pObservable)
            return;
    int newSize = ArraySize(m_onTickEventObservables) + 1;
    ArrayResize(m_onTickEventObservables, newSize);
    m_onTickEventObservables[newSize - 1] = pObservable;
    PrintFormat(">>> Attaching [%s] to OnTick callback.", pObservable.name());
}

CScalpingWatcher::~CScalpingWatcher() {
    ArrayFree(m_onTickEventObservables);
    ArrayFree(m_onBookEventObservables);
}

void CScalpingWatcher::addToOnBookEvent(Observable<IScalpingObserver> *pObservable) {
    for(int i = 0; i < ArraySize(m_onBookEventObservables); i++)
        if (m_onBookEventObservables[i] == pObservable)
            return;
    int newSize = ArraySize(m_onBookEventObservables) + 1;
    ArrayResize(m_onBookEventObservables, newSize);
    m_onBookEventObservables[newSize - 1] = pObservable;
    PrintFormat(">>> Attaching [%s] to OnBookEvent callback.", pObservable.name());
}


void CScalpingWatcher::observeOnTickEvent() {
    for (int i = 0; i < ArraySize(m_onTickEventObservables); i++)
        m_onTickEventObservables[i].observe();
}

void CScalpingWatcher::observeOnBookEvent() {
    for (int i = 0; i < ArraySize(m_onBookEventObservables); ++i)
        m_onBookEventObservables[i].observe();
}