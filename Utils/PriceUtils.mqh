#include <TapeReading/Core/CSymbol.mqh>

double PriceHigh(CSymbol &pSymbol, ENUM_TIMEFRAMES pTimeFrame) {
    double high_array[];
    CopyHigh(pSymbol.name(), pTimeFrame, 0, 1, high_array);
    return high_array[0];
}