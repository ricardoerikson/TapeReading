#include <TapeReading/Dom/CMarketSide.mqh>

class CBid: public CMarketSide
{
public:
    CBid(CMarketBook *pBook, const CSymbol &pSymbol, double pMaxDeviation);

    bool hasLiquidityVacuum(int pDepth);
    double accumulatedVolume(int pDepth);
    double averageVolume(int pDepth = 8);
    double bestPrice();
    double deviation(long pVolume);
    double maxVolume(int pDepth);
    double maxVolumePrice(int pDepth);
    double volume(int pPos);
    int maxVolumeIndex(int pDepth = 4);
};

CBid::CBid(CMarketBook *pBook, const CSymbol &pSymbol, double pMaxDeviation):
    CMarketSide(pBook, pSymbol, pMaxDeviation) {
}

double CBid::bestPrice() {
    if (exists())
        return SymbolInfoDouble(m_symbol.name(), SYMBOL_BID);
    return -1.0;
}

double CBid::deviation(long pVolume) {
    int bestAsk = bestIndex();
    int lastAsk = lastIndex();
    double averagePrice = 0.0;
    long volumeExec = pVolume;
    for (int i = bestAsk; i <= lastAsk; i++) {
        long currentVolume = m_marketBook.book[i].volume < volumeExec ? m_marketBook.book[i].volume : volumeExec;
        averagePrice += currentVolume * m_marketBook.book[i].price;
        volumeExec -= m_marketBook.book[i].volume;
        if (volumeExec <= 0)
            break;
    }
    if (volumeExec > 0)
        return -1.0;
    averagePrice /= (double) pVolume;
    return m_marketBook.book[bestAsk].price - averagePrice;
}

double CBid::volume(int pPos) {
    int requiredIndex = bestIndex() + pPos;
    if (exists() && requiredIndex >= bestIndex() && requiredIndex <= lastIndex())
        return (double) m_marketBook.book[requiredIndex].volume;
    return 0;
}

double CBid::averageVolume(int pDepth = 8) {
    double levels[];
    if (!exists())
        return -1;
    int begin = bestIndex();
    int end = MathMin(bestIndex() + pDepth - 1, lastIndex());
    if (exists()) {
        ArrayResize(levels, end - begin + 1);
        for (int i = begin; i <= end; i++)
            levels[i - bestIndex()] = (double) m_marketBook.book[i].volume;
    } else return -1;
    ArraySort(levels);
    double volumes[];
    if (ArraySize(levels) >= 6)
        ArrayCopy(volumes, levels, 0, 2, ArraySize(levels) - 4);
    else
        ArrayCopy(volumes, levels);
    return average(volumes);
}

double CBid::accumulatedVolume(int pDepth) {
    if (!exists())
        return -1;
    long sum = 0;
    int begin = bestIndex();
    int end = MathMin(bestIndex() + pDepth - 1, lastIndex());
    for (int i = begin; i <= end; i++)
        sum += m_marketBook.book[i].volume;
    return (double) sum;
}

bool CBid::hasLiquidityVacuum(int pDepth) {
    if (!exists())
        return true;
    int begin = bestIndex();
    int end = MathMin(bestIndex() + pDepth - 1, lastIndex());
    double sum = 0.0;
    for (int i = begin; i < end; i++)
        sum += MathAbs(m_marketBook.book[i].price - m_marketBook.book[i + 1].price);
    if (pDepth == (int)(sum / m_symbol.tickSize()))
        return false;
    return true;
}

int CBid::maxVolumeIndex(int pDepth) {
    if (!exists())
        return WRONG_VALUE;
    int begin = m_bestIndex;
    int end = MathMin(m_bestIndex + pDepth - 1, m_lastIndex);
    int index = WRONG_VALUE;
    long maxVol = 0.0;
    for(int i = end; i>= begin; i--) {
        if (m_marketBook.book[i].volume >= maxVol) {
            index = i;
            maxVol = m_marketBook.book[i].volume;
        }
    }
    return index;
}

double CBid::maxVolumePrice(int pDepth) {
    if (!exists())
        return 0.0;
    int index = maxVolumeIndex(pDepth);
    if (index != WRONG_VALUE)
        return m_marketBook.book[index].price;
    return 0.0;
}

double CBid::maxVolume(int pDepth) {
    if (!exists())
        return 0.0;
    int index = maxVolumeIndex(pDepth);
    if (index != WRONG_VALUE)
        return (double)m_marketBook.book[index].volume;
    return 0.0;
}
