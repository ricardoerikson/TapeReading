#include <TapeReading/Core/CAggression.mqh>

class CAggressionsBag
{
public:
    CAggressionsBag();
    ~CAggressionsBag();
    double mean() const;
    double slope() const;
    double volume() const;
    double low() const;
    double high() const;
    int count() const;
    void addAggression(CAggression &pAggression);
    void reset();
    CAggression last() const;

private:
    CAggression m_aggressions[];

};


CAggressionsBag::CAggressionsBag() {

}

CAggressionsBag::~CAggressionsBag() {
    ArrayFree(m_aggressions);
}

double CAggressionsBag::mean() const {
    if (ArraySize(m_aggressions) == 0)
        return 0;
    double sum = 0;
    double volume = 0;
    for(int i = 0; i < ArraySize(m_aggressions); ++i) {
        sum += m_aggressions[i].price() * m_aggressions[i].volume();
        volume += m_aggressions[i].volume();
    }
    return sum / volume;
}

double CAggressionsBag::slope() const {
    int size = ArraySize(m_aggressions);
    if (size == 0 || size == 1)
        return 0;

    double xb = 0;
    double yb = 0;
    for (int i = 0; i < size; ++i) {
        xb += i;
        yb += m_aggressions[i].price();
    }
    xb = xb / size;
    yb = yb / size;
    double lsxy = 0;
    double lsx = 0;
    for (int i = 0; i < size; ++i) {
        lsxy += (i - xb) * (m_aggressions[i].price() - yb);
        lsx += (i - xb) * (i - xb);
    }
    return lsxy / lsx;
}

void CAggressionsBag::addAggression(CAggression &pAggression) {
    int newSize = ArraySize(m_aggressions) + 1;
    ArrayResize(m_aggressions, newSize);
    m_aggressions[newSize - 1] = pAggression;
}

int CAggressionsBag::count() const {
    return ArraySize(m_aggressions);
}

double CAggressionsBag::volume() const {
    double sum = 0.0;
    for(int i = 0; i < ArraySize(m_aggressions); ++i) {
        sum += m_aggressions[i].volume();
    }
    return sum;
}

void CAggressionsBag::reset() {
    ArrayFree(m_aggressions);
}

double CAggressionsBag::low() const {
    if (count() == 0)
        return WRONG_VALUE;
    double low = DBL_MAX;
    for (int i = 0; i < count(); ++i) {
        if (m_aggressions[i].price() < low)
            low = m_aggressions[i].price();
    }
    return low;
}

double CAggressionsBag::high() const {
    if (count() == 0)
        return WRONG_VALUE;
    double high = 0.0;
    for(int i = 0; i < count(); ++i) {
        if (m_aggressions[i].price() > high)
            high = m_aggressions[i].price();
    }
    return high;
}

CAggression CAggressionsBag::last() const {
    if (count() == 0) {
        CAggression empty;
        return empty;
    }
    return m_aggressions[count() - 1];
}