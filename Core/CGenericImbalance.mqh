#include <TapeReading/Utils/MathUtils.mqh>

class CGenericImbalance
{
public:
    CGenericImbalance(const CGenericImbalance &pObject);
    CGenericImbalance(double pA, double pB, double pMinDiff = 0, double pRatio = 0);
    double value();
    double diff();
    double ratio();

private:
    double m_minDiff, m_ratio;
    double m_a, m_b;

};

CGenericImbalance::CGenericImbalance(const CGenericImbalance &pObject) {
    m_a       = pObject.m_a;
    m_b       = pObject.m_b;
    m_minDiff = pObject.m_minDiff;
    m_ratio   = pObject.m_ratio;
}

CGenericImbalance::CGenericImbalance(double pA, double pB, double pMinDiff, double pRatio) :
        m_a(pA),
        m_b(pB),
        m_minDiff(pMinDiff),
        m_ratio(pRatio) {
}

double CGenericImbalance::diff() {
    return MathAbs(m_a - m_b);
}

double CGenericImbalance::value() {
    if (m_minDiff != 0 && diff() < m_minDiff)
        return 0.0;
    if (m_ratio != 0 && ratio() < m_ratio)
        return 0.0;
    return Imbalance(m_a, m_b);
}

double CGenericImbalance::ratio() {
    double min = MathMin(m_a, m_b);
    double max = MathMax(m_a, m_b);
    return max / MathMax(min, 1);
}
