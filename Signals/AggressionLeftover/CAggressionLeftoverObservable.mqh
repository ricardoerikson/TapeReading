#include <TapeReading/Patterns/Observer/Observable.mqh>

#include <TapeReading/Dom/CDepthOfMarket.mqh>
#include <TapeReading/TimesNSales/CTimesNSales.mqh>
#include <TapeReading/Signals/AggressionLeftover/CAggressionLeftoverFIS.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>
#include <TapeReading/Utils/TickUtils.mqh>
#include <TapeReading/Utils/ArrayUtils.mqh>
#include <TapeReading/Core/Enums.mqh>

class CAggressionLeftoverObservable: public Observable<IScalpingObserver>
{
public:
    CAggressionLeftoverObservable(CTimesNSales *pTns, CDepthOfMarket *pDom, double pLeftoverFactor = 0.5);
    ~CAggressionLeftoverObservable();
    string name() const;
    void notifyObservers();

private:
    void notify(double pLeftover, double pExecutionPrice, AGGRESSION_TYPE pType);
    CTimesNSales *m_timesNSales;
    CDepthOfMarket *m_dom;
    const double m_leftoverFactor;
    double m_previousAsk, m_previousBid;
};

CAggressionLeftoverObservable::CAggressionLeftoverObservable(CTimesNSales *pTns, CDepthOfMarket *pDom, double pLeftoverFactor):
    m_leftoverFactor(pLeftoverFactor) {
    m_timesNSales = pTns;
    m_dom = pDom;
}

CAggressionLeftoverObservable::~CAggressionLeftoverObservable() {
}

string CAggressionLeftoverObservable::name() const {
    return "Aggression Leftover Observable";
}

void CAggressionLeftoverObservable::notifyObservers() {
    bool spreadClosed = ((m_dom.ask().bestPrice() - m_dom.bid().bestPrice()) == m_dom.symbol().tickSize());
    bool spreadChanged = (m_previousBid != m_dom.bid().bestPrice()) || (m_previousAsk != m_dom.ask().bestPrice());
    if (spreadClosed && spreadChanged) {
        MqlTick tick = m_timesNSales.trades[0];
        if (tick.last == m_dom.bid().bestPrice() && IsBuyAggression(tick)) {
            double askMean = m_dom.ask().averageVolume(8); // Checks mean volume for the contrary side
            double volume = m_dom.bid().volume(0);
            CAggressionLeftoverFIS fis(askMean, volume);
            if (fis.evaluate() >= m_leftoverFactor) {
                notify(volume, m_dom.bid().bestPrice(), BUY_AGGRESSION);
            }
        } else if (tick.last == m_dom.ask().bestPrice() && IsSellAggression(tick)) {
            double bidMean = m_dom.bid().averageVolume(8); // Checks mean volume for the contrary side
            double volume = m_dom.ask().volume(0);
            CAggressionLeftoverFIS fis(bidMean, volume);
            if (fis.evaluate() >= m_leftoverFactor) {
                notify(volume, m_dom.ask().bestPrice(), SELL_AGGRESSION);
            }
        }
    }
    m_previousAsk = m_dom.ask().bestPrice();
    m_previousBid = m_dom.bid().bestPrice();
}

void CAggressionLeftoverObservable::notify(double pLeftoverVolume, double pExecutionPrice, AGGRESSION_TYPE pType) {
    for (int i = 0; i < ArraySize(m_observers); i++) {
        switch(pType) {
            case BUY_AGGRESSION:
                m_observers[i].onLeftoverOnBid(pLeftoverVolume, pExecutionPrice);
                break;
            case SELL_AGGRESSION:
                m_observers[i].onLeftoverOnAsk(pLeftoverVolume, pExecutionPrice);
                break;
        }
    }
}