#include <TapeReading/Patterns/Observer/Observable.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>

#include <TapeReading/TimesNSales/CTimesNSales.mqh>
#include <TapeReading/Dom/CDepthOfMarket.mqh>
#include <TapeReading/Utils/TickUtils.mqh>

class CHiddenLiquidityObservable : public Observable<IScalpingObserver>
{
public:
    CHiddenLiquidityObservable(CTimesNSales *pTimesNSales, CDepthOfMarket *pDepthOfMarket);
    ~CHiddenLiquidityObservable(){};
    string name() const;
    void notifyObservers();

private:
    bool isAskIcebergBroken() const;
    bool isBidIcebergBroken() const;
    void notifyHiddenLiquidityAtAsk(MqlTick &pTick);
    void notifyHiddenLiquidityAtBid(MqlTick &pTick);

    CTimesNSales *m_timesNSales;
    CDepthOfMarket *m_dom;

    double m_priceAskIceberg;
    double m_priceBidIceberg;
};

CHiddenLiquidityObservable::CHiddenLiquidityObservable(CTimesNSales *pTimesNSales, CDepthOfMarket *pDepthOfMarket) {
    m_timesNSales = pTimesNSales;
    m_dom = pDepthOfMarket;
}

bool CHiddenLiquidityObservable::isAskIcebergBroken() const {
    if (m_priceAskIceberg != WRONG_VALUE && m_dom.ask().bestPrice() > m_priceAskIceberg)
        return true;
    return false;
}

bool CHiddenLiquidityObservable::isBidIcebergBroken() const {
    if (m_priceBidIceberg != WRONG_VALUE && m_dom.bid().bestPrice() < m_priceBidIceberg)
        return true;
    return false;
}

string CHiddenLiquidityObservable::name() const {
    return "Hidden Liquidity Observable";
}

void CHiddenLiquidityObservable::notifyObservers() {
    MqlTick lastTick = m_timesNSales.trades[0];
    if (IsBuyAggression(lastTick) && lastTick.last == m_dom.ask().bestPrice()) {
        double averageVolume = 2 * m_dom.ask().averageVolume(8);
        if (lastTick.volume > averageVolume) {
            notifyHiddenLiquidityAtAsk(lastTick);
        }
    } else if (IsSellAggression(lastTick) && lastTick.last == m_dom.bid().bestPrice()) {
        double averageVolume = 2 * m_dom.bid().averageVolume(8);
        if (lastTick.volume > averageVolume) {
            notifyHiddenLiquidityAtBid(lastTick);
        }
    }
}

void CHiddenLiquidityObservable::notifyHiddenLiquidityAtAsk(MqlTick &pTick) {
    m_priceAskIceberg = pTick.last;
    for (int i = 0; i < observersCount(); i++)
        observer(i).onHiddenLiquidityAtAsk(pTick.last, pTick.volume);
}

void CHiddenLiquidityObservable::notifyHiddenLiquidityAtBid(MqlTick &pTick) {
    m_priceBidIceberg = pTick.last;
    for (int i = 0; i < observersCount(); i++)
        observer(i).onHiddenLiquidityAtBid(pTick.last, pTick.volume);
}