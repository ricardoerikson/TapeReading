#include <TapeReading/Patterns/Observer/Observable.mqh>

#include <TapeReading/Dom/CDepthOfMarket.mqh>
#include <TapeReading/Signals/IScalpingObserver.mqh>

class CSpreadObservable : public Observable<IScalpingObserver>
{
public:
    CSpreadObservable(CDepthOfMarket *pDom);
    void notifyObservers();
    bool opened();
    bool changed();
    int elapsedTime();
    datetime lastChange();
    string name() const;

private:
    CDepthOfMarket *m_dom;
    datetime m_lastChange;
    bool m_changed;
    double m_lastAsk, m_lastBid;

};

CSpreadObservable::CSpreadObservable(CDepthOfMarket *pDom):
    m_lastAsk(0.0), m_lastBid(0.0) {
    m_dom = pDom;
}

string CSpreadObservable::name() const {
    return "Spread Observable";
}

void CSpreadObservable::notifyObservers() {
    if (opened()) {
        for (int i = 0; i < ArraySize(m_observers); i ++)
            m_observers[i].onSpreadOpened(m_dom.bid().bestPrice(), m_dom.ask().bestPrice());
    }
    if (m_dom.ask().bestPrice() != m_lastAsk && m_dom.bid().bestPrice() != m_lastBid) {
        int direction = 0;
        if (m_dom.ask().bestPrice() > m_lastAsk)
            direction = 1;
        else
            direction = -1;
        m_lastChange = TimeCurrent();
        m_lastAsk = m_dom.ask().bestPrice();
        m_lastBid = m_dom.bid().bestPrice();
        for (int i = 0; i < ArraySize(m_observers); i ++)
            m_observers[i].onSpreadChanged(m_lastChange, m_lastBid, m_lastAsk, direction);
        m_changed = true;
    } else {
        for (int i = 0; i < ArraySize(m_observers); i ++)
            m_observers[i].onSpreadAcceptance(m_lastChange, elapsedTime(), m_lastBid, m_lastAsk);
        m_changed = false;
    }
}

datetime CSpreadObservable::lastChange() {
    return m_lastChange;
}

int CSpreadObservable::elapsedTime() {
    return (int)(TimeCurrent() - m_lastChange);
}

bool CSpreadObservable::opened() {
    return m_dom.ask().bestPrice() - m_dom.bid().bestPrice() > m_dom.symbol().tickSize();
}