#include <TapeReading/Management/Trade/ECheckRetcode.mqh>
#include <TapeReading/Management/Trade/EStopOrderType.mqh>
#include <TapeReading/Core/CSymbol.mqh>

int CheckReturnCode(uint pRetCode) {
    int status;
    switch(pRetCode) {
        case TRADE_RETCODE_REQUOTE:
        case TRADE_RETCODE_CONNECTION:
        case TRADE_RETCODE_PRICE_CHANGED:
        case TRADE_RETCODE_TIMEOUT:
        case TRADE_RETCODE_PRICE_OFF:
        case TRADE_RETCODE_REJECT:
        case TRADE_RETCODE_ERROR:
            status = CHECK_RETCODE_RETRY;
            break;
        case TRADE_RETCODE_DONE:
        case TRADE_RETCODE_DONE_PARTIAL:
        case TRADE_RETCODE_PLACED:
        case TRADE_RETCODE_NO_CHANGES:
            status = CHECK_RETCODE_OK;
            break;
        default:
            status = CHECK_RETCODE_ERROR;
    }
    return status;
}

string CheckOrderType(ENUM_ORDER_TYPE pType) {
    switch(pType) {
        case ORDER_TYPE_BUY: return "BUY";
        case ORDER_TYPE_BUY_STOP: return "BUY STOP";
        case ORDER_TYPE_BUY_LIMIT: return "BUY LIMIT";
        case ORDER_TYPE_BUY_STOP_LIMIT: return "BUY STOP LIMIT";
        case ORDER_TYPE_SELL: return "SELL";
        case ORDER_TYPE_SELL_STOP: return "SELL STOP";
        case ORDER_TYPE_SELL_LIMIT: return "SELL LIMIT";
        case ORDER_TYPE_SELL_STOP_LIMIT: return "SELL STOP LIMIT";
    }
    return "INVALID ORDER TYPE";
}

double StopOrderPrice(CSymbol &pSymbol, ENUM_STOP_ORDER_TYPE pType, uint pLossTicks, double pOpenPrice) {
   if (pLossTicks <= 0) return 0;

    double stopPrice = 0.0;
    if (pType == BUY_STOP_TYPE)
        stopPrice = pOpenPrice - pLossTicks * pSymbol.tickSize();
    else if (pType == SELL_STOP_TYPE)
        stopPrice = pOpenPrice + pLossTicks * pSymbol.tickSize();
    return NormalizeDouble(MathAbs(stopPrice), pSymbol.digits());
}

double StopLossForBuyOrder(CSymbol &pSymbol, uint pLossTicks, double pOpenPrice) {
    return StopOrderPrice(pSymbol, BUY_STOP_TYPE, pLossTicks, pOpenPrice);
}

double StopLossForSellOrder(CSymbol &pSymbol, uint pLossTicks, double pOpenPrice) {
    return StopOrderPrice(pSymbol, SELL_STOP_TYPE, pLossTicks, pOpenPrice);
}

double TakeProfitForBuyOrder(CSymbol &pSymbol, uint pProfitTicks, double pOpenPrice) {
    return StopOrderPrice(pSymbol, SELL_STOP_TYPE, pProfitTicks, pOpenPrice);
}

double TakeProfitForSellOrder(CSymbol &pSymbol, uint pProfitTicks, double pOpenPrice) {
    return StopOrderPrice(pSymbol, BUY_STOP_TYPE, pProfitTicks, pOpenPrice);
}

double AdjustAboveStopLevel(CSymbol &pSymbol, double pPrice, uint pTicks = 3) {
    double currentPrice = SymbolInfoDouble(pSymbol.name(), SYMBOL_ASK);
    double stopPrice = currentPrice + pSymbol.stopLevel();
    double addPoints = pSymbol.tickSize() * pTicks;
    if (pPrice > stopPrice + addPoints) {
        return pPrice;
    } else {
        double newPrice = stopPrice + addPoints;
        Print("Price adjusted above stop level to "+ DoubleToString(newPrice));
        return newPrice;
    }
}

double AdjustBelowStopLevel(CSymbol &pSymbol, double pPrice, uint pTicks = 3) {
    double currentPrice = SymbolInfoDouble(pSymbol.name(), SYMBOL_BID);
    double stopPrice = currentPrice + pSymbol.stopLevel();
    double addPoints = pSymbol.tickSize() * pTicks;
    if (pPrice > stopPrice - addPoints) {
        return pPrice;
    } else {
        double newPrice = stopPrice - addPoints;
        Print("Price adjusted below stop level to "+ DoubleToString(newPrice));
        return newPrice;
    }
}