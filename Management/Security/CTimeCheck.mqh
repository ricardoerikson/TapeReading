#include <TapeReading/Management/Security/CSecurityCheck.mqh>
#include <TapeReading/Management/Trade/TradeReport.mqh>
#include <TapeReading/Utils/CDatetime.mqh>

class CTimeCheck : public CSecurityCheck
{
public:
    CTimeCheck(int pOpenHour, int pOpenMinute, int pCloseHour, int pCloseMinute);
    bool check();
    void report() const;

private:
    datetime m_opening;
    datetime m_closing;
};

CTimeCheck::CTimeCheck(int pOpenHour, int pOpenMinute, int pCloseHour, int pCloseMinute) :
    CSecurityCheck(TIME_CHECK) {
    m_opening = CDatetime::get(pOpenHour, pOpenMinute);
    m_closing = CDatetime::get(pCloseHour, pCloseMinute);
}

bool CTimeCheck::check() {
    datetime now = TimeCurrent();
    if (now < m_opening  || now > m_closing)
        return(false);
    return(true);
}

void CTimeCheck::report() const {
    string now = TimeToString(TimeCurrent(), TIME_SECONDS);
    string open = TimeToString(m_opening, TIME_SECONDS);
    string close = TimeToString(m_closing, TIME_SECONDS);
    ReportSecurityCheck(m_name, StringFormat("open time: %s, close time: %s, now: %s", open, close, now));
}